//Lets measure distance (height of an object)with distance sensor
//This can be used to measure Water level in our Tanks
//This code measures the distance of a solid object and displays in LCD screen

#include <Wire.h>
#include <LiquidCrystal_PCF8574.h>
LiquidCrystal_PCF8574 lcd(0x3F); 

#define trigPin 5
#define echoPin 6


void setup() {
  Serial.begin (9600);
  lcd.begin(16, 2);
  lcd.setBacklight(255);
  lcd.home();
  lcd.print("Hi SRB,Jos");
  delay(3000);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  lcd.clear();

}

void loop() {
  delay(100);
  int timetaken, dist;
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(1000);
  digitalWrite(trigPin, LOW);
  timetaken = pulseIn(echoPin, HIGH);
  dist = (timetaken / 2) * 0.034049 ;
  lcd.setCursor(0, 0);
  lcd.print("Distance in CM:");
  lcd.setCursor(0, 1);
  lcd.println(dist);
  delay(100);
  
}
