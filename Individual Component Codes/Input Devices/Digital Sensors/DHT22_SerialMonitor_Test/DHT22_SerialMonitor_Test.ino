/* This code explains the working of DHT22 sensor to measure Temperature & humidity values with SNT kit.
 *  We  can view values on Serial Monitor(ctrl+shift+M). 
 *  After uploading the code, please connect the Adaptor to plug & view the values on serial monitor.
*/
//1.ensure you have internet connection.
//2.Download these libraries and copy into Documents-->Arduino-->Libraries folder.
//3.Else, Go to Sketch->Include Library-> Managae Libraries and Type DHT sensor, Adafruit sensor to directly download

#include<DHT.h> // https://github.com/adafruit/DHT-sensor-library 
#include< Adafruit_Sensor.h> // https://github.com/adafruit/Adafruit_Sensor

DHT dht (7, DHT22); // 7 denotes Digital pin for DHT22 sensor, DHT22 denotes DHT type

void setup() {
  Serial.begin(9600);
  Serial.print("DHT test");
  dht.begin();
  delay(1000);
}

void loop() {
  delay(2000);
  float humidity = dht.readHumidity();
  float temperature = dht.readTemperature();

  Serial.print("\n Humidity =");
  Serial.print(humidity);
  Serial.print("% \n");
  Serial.print("Temperature =");
  Serial.print(temperature);
  Serial.print(" *C");
}
