/* This code explains the working of Light sensor with SNT kit.
 *  We can view values on Serial Monitor(ctrl+shift+M). 
 *  After uploading the code, please connect the Adaptor to plug & view the values on serial monitor.
 *  Pin mapping :
 *  Arduino--Light sensor (on the PCB)
    A0     --A1
    Gnd    --Gnd
    5V     --Vcc
*/
int ldr_pin = 1;
int ldr_value = 0;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);// To view the values on Serial monitor
  pinMode( ldr_pin, INPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  ldr_value = analogRead(ldr_pin);
  Serial.println("LDR Value: ");
  Serial.print(ldr_value);
  delay(1000); //Displays values after 1 sec
}
