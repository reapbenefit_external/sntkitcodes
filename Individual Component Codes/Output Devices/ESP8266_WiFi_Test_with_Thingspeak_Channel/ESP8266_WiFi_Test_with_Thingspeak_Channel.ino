#include <SoftwareSerial.h>

#define RX 11
#define TX 10

SoftwareSerial esp8266 (RX, TX);

String AP = "Reap_Benefit_2.4";       // CHANGE ME
String PASS = "solvesmalldentbig"; // CHANGE ME

String API = "";   // CHANGE ME
String HOST = "139.59.69.168";
String PORT = "3000";
String field = "field1";

int count = 0;

int countTrueCommand;
int countTimeCommand;
boolean found = false;
int valSensor = 0;

void setup() {
  Serial.begin(115200);
  esp8266.begin(115200);
  sendCommand("AT", 5, "OK");
  sendCommand("AT+CWMODE=1", 5, "OK");
  sendCommand("AT+CWJAP=\"" + AP + "\",\"" + PASS + "\"", 20, "OK");
}

void loop() {
  valSensor = analogRead(valSensor);
  Serial.print(valSensor);
  String Link = "GET /update?api_key=" + API  + "&field1=" ;
    Link = Link + valSensor ;
    Link = Link + " HTTP/1.1\r\n" + "Host: " + HOST + "\r\n" + "Connection: close\r\n\r\n";
    sendCommand("AT+CIPMUX=1", 5, "OK");
    sendCommand("AT+CIPSTART=0,\"TCP\",\"" + HOST + "\"," + PORT, 15, "OK");
    sendCommand("AT+CIPSEND=0," + String(Link.length() + 4), 4, ">");
    esp8266.println(Link);
    delay(1500);
    countTrueCommand++;
    sendCommand("AT+CIPCLOSE=0", 5, "OK");
    Serial.println(count);
    delay(1000);
 
}


void sendCommand(String command, int maxTime, char readReplay[]) {
  Serial.print(countTrueCommand);
  Serial.print(". at command => ");
  Serial.print(command);
  Serial.print(" ");
  while (countTimeCommand < (maxTime * 1))
  {
    esp8266.println(command);//at+cipsend
    if (esp8266.find(readReplay)) //ok
    {
      found = true;
      break;
    }

    countTimeCommand++;
  }

  if (found == true)
  {
    Serial.println("OYI");
    countTrueCommand++;
    countTimeCommand = 0;
  }

  if (found == false)
  {
    Serial.println("Fail");
    countTrueCommand = 0;
    countTimeCommand = 0;
  }

  found = false;
}
