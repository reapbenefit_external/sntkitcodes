#include <FastLED.h>

#define DATA_PIN 9
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];

void setup() {
  // put your setup code here, to run once:
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
  delay(1000);

}

void loop() {
  // put your main code here, to run repeatedly:
  FastLED.show();
  delay(1000);
  
  leds[0] = CRGB::Gray;
  FastLED.show();
  delay(1000);
  
  leds[0] = CRGB::Blue;
  FastLED.show();
  delay(1000);
  
  leds[0] = CRGB::Green;
  FastLED.show();
  delay(1000);
  
  leds[0] = CRGB::Black;
  FastLED.show();
  delay(1000);
}
