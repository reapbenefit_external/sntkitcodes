# SNT Kit Documentation


The Solve Ninja Techno Kit is designed to make technology accelerated problem solving by young citizens (we call them Solve Ninjas) easy.

Based on the Arduino board, this kit makes coding and electronics easy, so that you - the Solve Ninja -  can focus on problem-solving.

**Read the Wiki pages for help regarding the SNT Kit!**

## Wiki Contents:
#### **1. [Discover](https://gitlab.com/reapbenefit_external/sntkitcodes/wikis/Home/Discover)**

#### **2. [Investigate](https://gitlab.com/reapbenefit_external/sntkitcodes/wikis/Home/Investigate)**
   * [Input Sensors](https://gitlab.com/reapbenefit_external/sntkitcodes/wikis/Investigate/Input-Sensors)
       * [DHT - Digital Humidity and Temperature Sensor](https://gitlab.com/reapbenefit_external/sntkitcodes/wikis/Input-Sensors/DHT-Digital-Humidity-&-Temperature-Sensor)
       * [Distance Sensor](https://gitlab.com/reapbenefit_external/sntkitcodes/wikis/Input-Sensors/Distance-Sensor)
       * [Flow Sensor](https://gitlab.com/reapbenefit_external/sntkitcodes/wikis/Input-Sensors/Flow-Sensor)
       * [Moisture Sensor](https://gitlab.com/reapbenefit_external/sntkitcodes/wikis/Input-Sensors/Moisture-Sensor)
       * [Motion Sensor](https://gitlab.com/reapbenefit_external/sntkitcodes/wikis/Input-Sensors/Motion-Sensor)
       * [Light Sensor](https://gitlab.com/reapbenefit_external/sntkitcodes/wikis/Input-Sensors/Light-Sensor)

   * [Output Devices](https://gitlab.com/reapbenefit_external/sntkitcodes/wikis/Investigate/Output-Devices)
       * [LCD Screen](https://gitlab.com/reapbenefit_external/sntkitcodes/wikis/Output-Devices/LCD-Screen)
       * [Relay](https://gitlab.com/reapbenefit_external/sntkitcodes/wikis/Output-Devices/Relay)
       * [Multicolour LED](https://gitlab.com/reapbenefit_external/sntkitcodes/wikis/Output-Devices/Multicolour-LED)
       * [Buzzer](https://gitlab.com/reapbenefit_external/sntkitcodes/wikis/Output-Devices/Buzzer)

   * [External Components](https://gitlab.com/reapbenefit_external/sntkitcodes/wikis/Investigate/External-Components)
       * [Sharp Dust sensor](https://gitlab.com/reapbenefit_external/sntkitcodes/wikis/External-Components/Sharp-Dust-Sensor)
       * [Honeywell PM sensor](https://gitlab.com/reapbenefit_external/sntkitcodes/wikis/External-Components/Honeywell-PM-Sensor)
       * [SIM800Module](https://gitlab.com/reapbenefit_external/sntkitcodes/wikis/External-Components/SIM800-Module)



#### **3. [Solve](https://gitlab.com/reapbenefit_external/sntkitcodes/wikis/Home/Solve)**
   * [Blink Example](https://gitlab.com/reapbenefit_external/sntkitcodes/wikis/Solve/Blink-Example) 
   * [Pin Configuration](https://gitlab.com/reapbenefit_external/sntkitcodes/wikis/Solve/Pin-Configuration)
   * [Libraries](https://gitlab.com/reapbenefit_external/sntkitcodes/wikis/Solve/Libraries)
   * [Example Problem Statements](https://gitlab.com/reapbenefit_external/sntkitcodes/wikis/Solve/Example-Problem-Statements)
   * [Example Prototypes](https://gitlab.com/reapbenefit_external/sntkitcodes/wikis/Solve/Example-Prototypes)
   * [Troubleshoot](https://gitlab.com/reapbenefit_external/sntkitcodes/wikis/Solve/Troubleshoot)

#### **4. Share**

[//]: # (Solve Ninja App)

[//]: # (Gitlab)

[//]: # (Instructable)

[//]: # (Meet us!)