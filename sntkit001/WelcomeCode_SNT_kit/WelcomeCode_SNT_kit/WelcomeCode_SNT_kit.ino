/*
   Unified code for the SolveNinja TechnoTiger Kit

   1 Hardware
   1.1 Design philosophy - use common off the shelf components to make replacement and reuse ease
   1.2 Components
    MCU - Arduino Uno
    Communications - LCD(i2C enabled), RGB (WS821B), Buzzer, ESP8266 for wifi, Relay
    Input Sensors - DHT, Flow, Distance, Soil, Current, LDR
   1.3 PinMapping
    A0 - Soil Sensor
    A1 - LDR
    D2 - Flow sensor
    D3 - PIR sensor
    D7 - DHT
    D4 - Relay
    D5 - Ultrasonic Trigger
    D6 - Ultrasonic Echo
    D8 - Buzzer
    D9 - Multicolor LED
    D11 - ESP8266 RX
    D12 - ESP8266 TX

   2 Code Structure
    Limit global variables, prefer local variables
    create subfunctions, and call in functions
    have a test and deployment subfunction, so that testing becomes the cutlure #failfastlearnfaster
    List libraries used, with exact Search names from library manager to make setup easy
    Include drivers to be installed, if any are custom. Prefer standardised components, which are part of the Arduino IDE

   2.1 Libraries used:
   Adafruit Unified Driver
*/

////////////////////////////////////////////////////////

////////////////Libraries///////////////////////////////
#include <DHT.h>
#include <DHT_U.h>
#include <Adafruit_Sensor.h>
#include <Wire.h>
#include <LiquidCrystal_PCF8574.h>
#include <SoftwareSerial.h>
#include <FastLED.h>

/////////////////// Component Setup /////////////////////
//Wifi Setup
SoftwareSerial esp8266(11, 10);
LiquidCrystal_PCF8574 lcd(0x27); //Else change to 0x3F

int soil_pin = 0; //Analog pin A0, Moisture Sensor
int ldr_pin = 1; //Analog pin A1, LDR
int current_pin = 2; //Analog pin 2, Current Sensor
byte flow_pin = 2; //Digital pin 2, FLow Sensor
int pir_pin = 3;    //Digital pin 3, PIR Sensor
#define relay_pin 4 //Digital pin 4, Relay 
#define distance_trigger_pin 5 //Digital pin 5, Distance Sensor
#define distance_echo_pin 6 //Digital pin 6, Distance Sensor
#define dht_pin 7 //Digital pin 7, DHT Temp+Humidity Sensor
#define buzzer_pin 8 //Digital pin 8, Buzzer
#define DATA_PIN 9// Digital pin number for LED (NeoPixel)
#define led_pin 13 //Digital pin 13, LED on PCB

#define NUM_LEDS 3

// Define the array of leds
CRGB leds[NUM_LEDS];

/////////////////Variables//////////////////////////////

/*Inputs*/
//Soil
int moisture_value = 0;

//LDR
int ldr_value = 0;

//Current Sensor
int current_value = 0;

//Water flow
int count = 0;
byte sensorInterrupt = 0;  // 0 = digital pin 2
float calibrationFactor = 4.5;
volatile byte pulseCount = 0;
float flowRate = 0.0;
unsigned int flowMilliLitres = 0;
unsigned long totalMilliLitres = 0;
unsigned long oldTime = 0;

//PIR sensor
int calibrationTime = 30;
long unsigned int lowIn;
long unsigned int pause = 5000;
boolean lockLow = true;
boolean takeLowTime;

//DHT
#define dht_type DHT22
int t, h;
DHT dht(dht_pin, dht_type);

//Distance
long duration;
int distance;

/*Output*/
String lcd_address;

//WiFi
enum {WIFI_ERROR_NONE = 0, WIFI_ERROR_AT, WIFI_ERROR_RST, WIFI_ERROR_SSIDPWD, WIFI_ERROR_SERVER, WIFI_ERROR_UNKNOWN};
#define BUFFER_SIZE 128
#define SSID  "ReapBenefit_Act_Upstairs"   // change this to match your WiFi SSID
#define PASS  "solvesmalldentbig"  // change this to match your WiFi password
#define PORT  "8080"      // using port 8080 by default
char buffer[BUFFER_SIZE];
unsigned long time;


void setup() {
  // put your setup code here, to run once:
  lcd.begin(16, 2);
  lcd.setBacklight(255);
  lcd.home();
  lcd.clear();
  lcd.print("Hi Solve Ninjas");
  //delay(2000);

  Serial.begin(9600);
  Serial.println(F("Hi Solve Ninja, What problem are you Solving Today?"));
  delay(2000);
  //Wire.begin();
  pinMode(soil_pin, INPUT);
  pinMode(soil_pin, LOW);
  pinMode(ldr_pin, INPUT);
  pinMode(ldr_pin, LOW);
  pinMode(current_pin, INPUT);
  pinMode(flow_pin, INPUT);
  pinMode(pir_pin, INPUT);
  digitalWrite(pir_pin, LOW);
  pinMode(dht_pin, INPUT);
  pinMode(distance_echo_pin, INPUT);
  pinMode(distance_trigger_pin, OUTPUT);
  pinMode(relay_pin, OUTPUT);
  pinMode(buzzer_pin, OUTPUT);
  pinMode(led_pin, OUTPUT);

  //lcd.print("See RGB LED on Right");
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
  delay(1000);
  lcd.clear();
  lcd.print("Starting Sensor Tests");
  delay(1000);
  //lcd.clear();
  test();
  dht.begin();
  Serial.println("Started");
  lcd.print("Starting WiFi");
  delay(1000);

  esp8266.begin(9600);

  lcd.clear();
}

/////////////////////////////////////////////////
void loop() {
  // put your main code here, to run repeatedly:
  deploy();
  //wifi_module();
}

void test() {
  test_lcd();
  test_led();
  test_multicolorled();
  test_buzzer();
  test_soil();
  test_ldr();
  test_relay();
  test_flow();
  test_dht();
  test_distance();
}

void deploy() {
  //Add your code here as shown in the example below//
  //  test_ldr();
  //  delay(1000);
  test_dht();
  delay(1000);

}

////////////////////////////////////////////////////
void test_led() {
  lcd.clear();
  lcd.print("See LED D13 ON");
  digitalWrite(led_pin, HIGH);
  Serial.println("LED is ON");
  delay(2000);
  lcd.clear();
  lcd.print("See LED D13 OFF");
  digitalWrite(led_pin, LOW);
  Serial.println("LED is OFF");
  delay(2000);

}
float test_soil() {
  //Measures the mositure of any substance
  lcd.clear();
  moisture_value = analogRead(soil_pin);
  //moisture_value = map(moisture_value, 1023, 0, 0, 0);
  Serial.println("Mositure : ");
  Serial.print(moisture_value);
  lcd.print("Mositure : ");
  lcd.print(moisture_value);
  return (moisture_value);
  delay(1000);
}

void test_ldr() {
  //Measures the intensity of light
  lcd.clear();
  ldr_value = analogRead(ldr_pin);
  lcd.setCursor(0, 0);
  lcd.print("LightSensor val:");
  Serial.println("Light Sensor value is: ");
  Serial.print(ldr_value);
  lcd.setCursor(0, 1);
  lcd.print(ldr_value);
  delay(2000);
  lcd.clear();
}
void test_current() {
  //Measures the current

}
void test_flow() {

  if ((millis() - oldTime) > 1000) {
    count = ++count;
    detachInterrupt(sensorInterrupt);
    flowRate = ((1000.0 / (millis() - oldTime)) * pulseCount) / calibrationFactor;
    oldTime = millis();
    flowMilliLitres = (flowRate / 60) * 1000;
    totalMilliLitres += flowMilliLitres;
    unsigned int frac;

    // Print the flow rate for this second in litres / minute
    Serial.print("Flow rate: ");
    Serial.print(int(flowRate));  // Print the integer part of the variable
    Serial.print(".");             // Print the decimal point
    // Determine the fractional part. The 10 multiplier gives us 1 decimal place.
    frac = (flowRate - int(flowRate)) * 10;
    Serial.print(frac, DEC) ;      // Print the fractional part of the variable
    Serial.print("L/min");
    // Print the number of litres flowed in this second
    Serial.print("  Current Liquid Flowing: ");             // Output separator
    Serial.println(flowMilliLitres);
    Serial.println("mL/Sec");

    // Print the cumulative total of litres flowed since starting
    Serial.print("  Output Liquid Quantity: ");             // Output separator
    lcd.setCursor(0, 1);
    lcd.println(totalMilliLitres);
    Serial.println(totalMilliLitres);
    Serial.println("mL");
    // Reset the pulse counter so we can start incrementing again
    pulseCount = 0;
    // Enable the interrupt again now that we've finished sending output
    attachInterrupt(sensorInterrupt, pulseCounter, FALLING);
  }
  delay(1000);
}

void test_dht() {
  //Measures the Temperature & Humidity
  lcd.clear();
  lcd.print("Reading Temp");
  lcd.clear();
  delay (1000);
  h = dht.readHumidity();
  t = dht.readTemperature();
  delay (1000);
  h = dht.readHumidity();
  t = dht.readTemperature();
  Serial.println("Temperature:");
  Serial.print(t);
  lcd.setCursor(0, 0);
  lcd.print("Temp(C):");
  lcd.print(t);
  Serial.println("Humidity:");
  Serial.print(h);
  lcd.setCursor(0, 1);
  lcd.print("Hum(%):");
  lcd.print(h);
  delay(1000);
  lcd.clear();
}
void test_pir() {
  Serial.print("calibrating sensor ");
  for (int i = 0; i < calibrationTime; i++) {
    Serial.print(".");
    delay(1000);
  }
  Serial.println(" done");
  Serial.println("SENSOR ACTIVE");
  if (digitalRead(pir_pin) == HIGH) {

    if (lockLow) {
      //makes sure we wait for a transition to LOW before any further output is made:
      lockLow = false;
      Serial.println("---");
      Serial.print("motion detected at ");
      Serial.print(millis() / 1000);
      Serial.println(" sec");
      delay(50);
    }
    takeLowTime = true;
  }

  if (digitalRead(pir_pin) == LOW) {

    if (takeLowTime) {
      lowIn = millis();          //save the time of the transition from high to LOW
      takeLowTime = false;       //make sure this is only done at the start of a LOW phase
    }
    //if the sensor is low for more than the given pause,
    //we assume that no more motion is going to happen
    if (!lockLow && millis() - lowIn > pause) {
      //makes sure this block of code is only executed again after
      //a new motion sequence has been detected
      lockLow = true;
      Serial.print("motion ended at ");      //output
      Serial.print((millis() - pause) / 1000);
      Serial.println(" sec");
      delay(50);
    }
  }
}

void test_relay() {
  lcd.clear();
  //Turns the Relay ON and OFF for 2 seconds
  digitalWrite(relay_pin, HIGH);
  Serial.println("Relay is ON");
  lcd.print("Relay ON");
  delay(2000);
  lcd.clear();
  lcd.print("Relay Off");
  digitalWrite(relay_pin, LOW);
  Serial.println("Relay is OFF");
  delay(2000);
  lcd.clear();
}

void test_distance() {
  //Measures the Distance of an solid object in centimetres(CM)
  lcd.setCursor(0, 0);
  lcd.print("Distanc-ing!!");
  // Clears the trigPin
  digitalWrite(distance_trigger_pin, LOW);
  delayMicroseconds(2);
  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(distance_trigger_pin, HIGH);
  delayMicroseconds(10);
  digitalWrite(distance_trigger_pin, LOW);
  // Reads the echoPin, returns the sound wave travel time in microseconds
  duration = pulseIn(distance_echo_pin, HIGH);
  // Calculating the distance
  distance = duration * 0.034 / 2;
  // Prints the distance on the Serial Monitor
  Serial.print("Distance: ");
  Serial.println(distance);
  lcd.setCursor(0, 1);
  lcd.print("Distance: ");
  lcd.print(distance);
  delay(1000);
  lcd.clear();
}
void test_buzzer() {
  //Buzzer beeps ON & OFF for 2 seconds
  lcd.clear();
  lcd.print("Hear Buzzer!");
  Serial.print("Buzzer beeps");
  digitalWrite(buzzer_pin, HIGH);
  delay(2000);
  lcd.clear();
  lcd.print("Buzzer is Off!");
  Serial.println("Buzzer stops");
  digitalWrite(buzzer_pin, LOW);
  delay(1000);
  lcd.clear();
}

void test_lcd() {
  byte error, address;
  int nDevices;
  Serial.println("Scanning...");
  nDevices = 0;
  for (address = 1; address < 127; address++ )
  {

    Wire.beginTransmission(address);
    error = Wire.endTransmission();

    if (error == 0)
    {
      Serial.print("I2C device found at address 0x");
      if (address < 16)
        Serial.print("0");
      Serial.print(address, HEX);
      Serial.println("  !");
      lcd.clear();
      lcd.print("LCD on 0X");
      lcd.print(address, HEX);
      delay(1000);

      nDevices++;
      lcd.clear();
    }
    else if (error == 4)
    {
      Serial.print("Unknown error at address 0x");
      if (address < 16)
        Serial.print("0");
      Serial.println(address, HEX);
    }
  }
  if (nDevices == 0)
    Serial.println("No I2C devices found\n");
  else
    Serial.println("done\n");

  //delay(5000);           // wait 5 seconds for next scan
  while (! Serial);
  Serial.println("Dose: check for LCD");
  //Wire.begin();

  //Wire.beginTransmission(0x3F);
  //error = Wire.endTransmission();
  Serial.print("Error: ");
  Serial.print(error);
  if (error == 0) {
    Serial.println(": LCD found.");
  } else {
    Serial.println(": LCD not found.");
  }
}

void test_multicolorled() {
  lcd.clear();
  lcd.print("Red");
  leds[0] = CRGB::Red;
  FastLED.show();
  delay(1000);
  lcd.print("Grey");
  leds[0] = CRGB::Gray;
  FastLED.show();
  delay(1000);
  lcd.print("Blue");
  leds[0] = CRGB::Blue;
  FastLED.show();
  delay(1000);
  lcd.print("Green");
  leds[0] = CRGB::Green;
  FastLED.show();
  delay(1000);
  lcd.clear();
  lcd.print("..Off..");
  leds[0] = CRGB::Black;
  FastLED.show();
  delay(1000);
  lcd.clear();
}

void wifi_module() {
  int ch_id, packet_len;
  char *pb;
  esp8266.readBytesUntil('\n', buffer, BUFFER_SIZE);
  if (strncmp(buffer, "+IPD,", 5) == 0) {          //Compara.
    // request: +IPD,ch,len:data
    sscanf(buffer + 5, "%d,%d", &ch_id, &packet_len);
    if (packet_len > 0) {
      // read serial until packet_len character received
      // start from :
      pb = buffer + 5;
      while (*pb != ':') pb++;
      pb++;
      if (strncmp(pb, "GET /", 5) == 0) {
        serve_homepage(ch_id);
      }
    }
  }
  delay(1000);
}

/*Insterrupt Service Routine for Flow sensor*/
void pulseCounter() {
  // Increment the pulse counter
  pulseCount++;
}

void serve_homepage(int ch_id) {
  //String header = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\nConnection: close\r\n";
  String header = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\nConnection: close\r\nRefresh: 20\r\n";
  String content = "Hello ESP!!!: Sono attivo da: ";
  time = millis() / 1000;
  content += time;
  content += "secondi!!!";
  /*
    // output the value of each analog input pin
    for (int analogChannel = 0; analogChannel < 2; analogChannel++) {
      int sensorReading = analogRead(analogChannel);
      content += "analog input ";
      content += analogChannel;
      content += " is ";
      content += sensorReading;
      content += "<br />\n";
    } */

  header += "Content-Length:";
  header += (int)(content.length());
  header += "\r\n\r\n";
  esp8266.print("AT+CIPSEND=");
  esp8266.print(ch_id);
  esp8266.print(",");
  esp8266.println(header.length() + content.length());
  if (esp8266.find(">")) {
    esp8266.print(header);
    esp8266.print(content);
    delay(20);
  }

  /*Serial.print("AT+CIPCLOSE=");
    Serial.println(ch_id);*/
}
//-------------------------------
byte setupWiFi() {
  esp8266.println("AT");
  delay(500);
  if (!esp8266.find("OK")) {
    delay(300);
    return WIFI_ERROR_AT;
  }
  //delay(1500);
  // reset WiFi module
  esp8266.println("AT+RST");
  delay(500);
  if (!esp8266.find("ready")) {
    delay(300);
    return WIFI_ERROR_RST;
  }
  delay(500);

  // set mode 3
  esp8266.print("AT+CWJAP=\"");
  esp8266.print(SSID);
  esp8266.print("\",\"");
  esp8266.print(PASS);
  esp8266.println("\"");
  delay(2000);
  if (!esp8266.find("OK")) {
    delay(300);
    return WIFI_ERROR_SSIDPWD;
  }
  delay(500);
  // start server
  esp8266.println("AT+CIPMUX=1");
  delay(500);
  if (!esp8266.find("OK")) {
    delay(200);
    return WIFI_ERROR_SERVER;
  }
  delay(500);
  esp8266.print("AT+CIPSERVER=1,"); // turn on TCP service
  delay(500);
  esp8266.println(PORT);
  delay(500);
  if (!esp8266.find("OK")) {
    delay(200);
    return WIFI_ERROR_SERVER;
  }
  return WIFI_ERROR_NONE;
}
