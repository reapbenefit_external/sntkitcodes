#include <BlynkSimpleEsp8266.h> //include Blynk library
#include <DHT.h> //include temp sensor library
#define dht_dpin 7 // Digital Pin sensor is connected to pin 4 
dht DHT;
char auth[] = "********************"; //Enter the authentication code
char ssid[] = "**********"; //Enter the name of your wifi
char pass[] = "**********"; //Enter password of your wifi
void setup() {
  Blynk.begin(auth, ssid, pass);
  Serial.begin(9600);
  delay(500); //Delay to let system boot
}
void loop()
{
  Blynk.run();
  Blynk.syncAll();
  Blynk.virtualWrite(V0, DHT.temperature); //write value of temp to V0 pin
  Blynk.virtualWrite(V1, DHT.humidity); //write value of humidity to !1 pin
  DHT.read22(dht_apin);
}
