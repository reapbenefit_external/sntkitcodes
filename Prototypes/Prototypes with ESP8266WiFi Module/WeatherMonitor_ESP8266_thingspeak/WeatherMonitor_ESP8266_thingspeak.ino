/*Weather Monitor
   Input Sensor:DHT22
   Output Sensor :ESP8266 WiFi Module
   Libraries: SoftwareSerial(inbuilt), DHT sensor Library
   Uploads Data in Thinspeak channel.Do create a thingspeak channel via www.thingspeak.com
*/
#include <SoftwareSerial.h>
#include<DHT.h>
#define RX 11
#define TX 10
SoftwareSerial esp8266 (RX, TX);

//Wifi credentials
String AP = "xxxxx";       // CHANGE ME
String PASS = "xxxxxxx"; // CHANGE ME
//Create channel in Thinsgpeak
String API = "xxxxxx";   // CHANGE ME
String HOST = "api.thingspeak.com";
String PORT = "80";
String field1 = "field1";
String field2 = "field2";

DHT dht (7, DHT22);
float humidity, temperature;

int countTrueCommand;
int countTimeCommand;
boolean found = false;


void setup() {
  Serial.begin(115200);
  dht.begin();
  esp8266.begin(115200);
  sendCommand("AT", 5, "OK");
  sendCommand("AT+CWMODE=1", 5, "OK");
  sendCommand("AT+CWJAP=\"" + AP + "\",\"" + PASS + "\"", 20, "OK");
}

void loop() {
  temp();
  String getData = "GET /update?api_key=" + API + "&" + field1 + "=" + String(humidity) + "&" + field2 + "=" + String(temperature);
  sendCommand("AT+CIPMUX=1", 5, "OK");
  sendCommand("AT+CIPSTART=0,\"TCP\",\"" + HOST + "\"," + PORT, 15, "OK");
  sendCommand("AT+CIPSEND=0," + String(getData.length() + 4), 4, ">");
  esp8266.println(getData);
  delay(1500);
  countTrueCommand++;
  sendCommand("AT+CIPCLOSE=0", 5, "OK");
  delay(20000);
}
void temp() {
  delay(2000);
  humidity = dht.readHumidity();
  temperature = dht.readTemperature();

  Serial.print("\n Humidity =");
  Serial.print(humidity);
  Serial.print("% \n");
  Serial.print("Temperature =");
  Serial.print(temperature);
  Serial.print(" *C");
}

void sendCommand(String command, int maxTime, char readReplay[]) {
  Serial.print(countTrueCommand);
  Serial.print(". at command => ");
  Serial.print(command);
  Serial.print(" ");
  while (countTimeCommand < (maxTime * 1))
  {
    esp8266.println(command);//at+cipsend
    if (esp8266.find(readReplay)) //ok
    {
      found = true;
      break;
    }

    countTimeCommand++;
  }

  if (found == true)
  {
    Serial.println("OK");
    countTrueCommand++;
    countTimeCommand = 0;
  }

  if (found == false)
  {
    Serial.println("Fail");
    countTrueCommand = 0;
    countTimeCommand = 0;
  }

  found = false;
}
