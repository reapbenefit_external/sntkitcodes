/*Weather Monitor
   DHT22 and ESP8266 WiFi Module
   Uplkoads Data in Thinspeak channel.
   Do create a thingspeak channel via www.thingspeak.com
*/
#include <SoftwareSerial.h>
#include<DHT.h>
#define RX 11
#define TX 10
SoftwareSerial esp8266 (RX, TX);
#include<FastLED.h>
#include <Wire.h>
#include <LiquidCrystal_PCF8574.h>
LiquidCrystal_PCF8574 lcd(0x3f);
#define ldr_pin 1 //analog pin 1
int value_ldr;
#define DATA_PIN 9 // Digital pin number for LED (Multicolor)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];
//Wifi credentials
String AP = "Reap benefit Airtel";       // CHANGE ME
String PASS = "solvesmalldentbig"; // CHANGE ME
//Create channel in Thinsgpeak
String API = "WDQSSCG7DDR0H23F";   // CHANGE ME
String HOST = "api.thingspeak.com";
String PORT = "80";
String field1 = "field1";
String field2 = "field2";
String field3 = "field3";
DHT dht (7, DHT22);
float humidity, temperature;

int countTrueCommand;
int countTimeCommand;
boolean found = false;


void setup() {
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
  delay(1000);
  lcd.begin(16, 2);
  lcd.setBacklight(255);
  Serial.begin(115200);
  dht.begin();
  pinMode(ldr_pin, INPUT);
  esp8266.begin(115200);
  sendCommand("AT", 5, "OK");
  sendCommand("AT+CWMODE=1", 5, "OK");
  sendCommand("AT+CWJAP=\"" + AP + "\",\"" + PASS + "\"", 20, "OK");
}

void loop() {
  temp();
  lcd_display();
  ldrvalue();
  String getData = "GET /update?api_key=" + API +
                   "&" + field1 + "=" + String(humidity) +
                   "&" + field2 + "=" + String(temperature) +
                   "&" + field3 + "=" + String(value_ldr);
  sendCommand("AT+CIPMUX=1", 5, "OK");
  sendCommand("AT+CIPSTART=0,\"TCP\",\"" + HOST + "\"," + PORT, 15, "OK");
  sendCommand("AT+CIPSEND=0," + String(getData.length() + 4), 4, ">");
  esp8266.println(getData);
  delay(1500);
  countTrueCommand++;
  sendCommand("AT+CIPCLOSE=0", 5, "OK");
  delay(20000);
}
void ldrvalue() {
  value_ldr = analogRead(ldr_pin); // read LDR value
  Serial.println(value_ldr);
  delay(2000);

}
void temp() {
  delay(2000);
  humidity = dht.readHumidity();
  temperature = dht.readTemperature();

  Serial.print("\n Humidity =");
  Serial.print(humidity);
  Serial.print("% \n");
  Serial.print("Temperature =");
  Serial.print(temperature);
  Serial.print(" *C");
}
void lcd_display() {
  lcd.setCursor(0, 0);
  lcd.print("T(C):");
  lcd.print(temperature);
  lcd.setCursor(8, 0);
  lcd.print(" H(%):");
  lcd.print(humidity);
  lcd.setCursor(0, 1);
  lcd.print("Lux:");
  lcd.print(value_ldr);

}

void sendCommand(String command, int maxTime, char readReplay[]) {
  Serial.print(countTrueCommand);
  Serial.print(". at command => ");
  Serial.print(command);
  Serial.print(" ");
  while (countTimeCommand < (maxTime * 1))
  {
    esp8266.println(command);//at+cipsend
    if (esp8266.find(readReplay)) //ok
    {
      found = true;
      break;
    }

    countTimeCommand++;
  }

  if (found == true)
  {
    Serial.println("OK");
    countTrueCommand++;
    countTimeCommand = 0;
  }

  if (found == false)
  {
    Serial.println("Fail");
    countTrueCommand = 0;
    countTimeCommand = 0;
  }

  found = false;
}
