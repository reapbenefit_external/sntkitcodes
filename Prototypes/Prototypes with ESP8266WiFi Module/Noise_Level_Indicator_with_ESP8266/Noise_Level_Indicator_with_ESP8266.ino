/* Prototype Name : Know the Noise Level with ESP8266 and SNT kit
   Input Sensor   : Analog Sound Level sensor
   Output Device  : Multicolor LED, LiquidCrystalPCF584
   Libraries Used : LiquidCrystalPCF584
   FastLED to turn off from use
*/

#include <LiquidCrystal_PCF8574.h>
LiquidCrystal_PCF8574 lcd(0x27);

#include <FastLED.h>
#define DATA_PIN 9// Digital pin number for LED (NeoPixel)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];

#include <SoftwareSerial.h>

#define RX 11
#define TX 10

SoftwareSerial esp8266 (RX, TX);

String AP = "Reap_Benefit_2.4";       // CHANGE ME
String PASS = "solvesmalldentbig"; // CHANGE ME

String API = "5X8JKCV7OVKN1DYA";   // CHANGE ME
String HOST = "139.59.69.168";
String PORT = "3000";
String field = "field3";

int count = 0;
int countTrueCommand;
int countTimeCommand;
boolean found = false;

//dB values
const float dBAnalogQuiet = 10; // calibrated value from analog input (48 dB equivalent)
const float dBAnalogMedium = 17;
const float dBAnalogLoud = 30;
#define PIN_ANALOG_IN A2
int value;

void setup() {
  Serial.begin(115200) ;
  pinMode(A2, INPUT);//delay (1000);
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
  lcd.begin(16, 2);
  lcd.setBacklight(255);
  lcd.home(); lcd.clear();
  lcd.print("Noise Level ");

  esp8266.begin(115200);
  sendCommand("AT", 5, "OK");
  sendCommand("AT+CWMODE=1", 5, "OK");
  sendCommand("AT+CWJAP=\"" + AP + "\",\"" + PASS + "\"", 20, "OK");

}

void loop() {
  //value = noise_test();
  delay(1000);
  leds[0] = CRGB::Black;
  FastLED.show();
  value = analogRead(PIN_ANALOG_IN);
  Serial.print(value);
  //Decibels values
  float decibelsValueQuiet = 49;
  float decibelsValueMedium = 70;
  float decibelsValueLoud = 97;


  if (value <= 0)
  {
    decibelsValueQuiet = 0 ;
    printDecibels("Quiet", decibelsValueQuiet);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Quiet:");
    lcd.setCursor(0, 1);
    lcd.print(decibelsValueQuiet);
    //delay (1000);
  }

  else if (value > 0 && value < 13)
  {
    decibelsValueQuiet += 20 * log10(value / dBAnalogQuiet);
    printDecibels("Quiet", decibelsValueQuiet);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Quiet:");
    lcd.setCursor(0, 1);
    lcd.print(decibelsValueQuiet);
    //delay (1000);
    String Link = "GET /update?api_key=" + API  + "&field3=" ;
    Link = Link + decibelsValueQuiet ;
    Link = Link + " HTTP/1.1\r\n" + "Host: " + HOST + "\r\n" + "Connection: close\r\n\r\n";
    sendCommand("AT+CIPMUX=1", 5, "OK");
    sendCommand("AT+CIPSTART=0,\"TCP\",\"" + HOST + "\"," + PORT, 15, "OK");
    sendCommand("AT+CIPSEND=0," + String(Link.length() + 4), 4, ">");
    esp8266.println(Link);
    delay(1500);
    countTrueCommand++;
    sendCommand("AT+CIPCLOSE=0", 5, "OK");
    Serial.println(count);
    delay(1000);
  }
  else if ((value > 13) && ( value <= 23) )
  {
    decibelsValueMedium += log10(value / dBAnalogMedium);
    printDecibels("Medium", decibelsValueMedium);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Medium:");
    lcd.setCursor(0, 1);
    lcd.print(decibelsValueMedium);
    //delay ( 1000);
    String Link = "GET /update?api_key=" + API  + "&field3=" ;
    Link = Link + decibelsValueMedium ;
    Link = Link + " HTTP/1.1\r\n" + "Host: " + HOST + "\r\n" + "Connection: close\r\n\r\n";
   // String getData = "GET /update?api_key=" + API + "&" + field + "=" + String(decibelsValueMedium);
    sendCommand("AT+CIPMUX=1", 5, "OK");
    sendCommand("AT+CIPSTART=0,\"TCP\",\"" + HOST + "\"," + PORT, 15, "OK");
    sendCommand("AT+CIPSEND=0," + String(Link.length() + 4), 4, ">");
    esp8266.println(Link);
    delay(1500);
    countTrueCommand++;
    sendCommand("AT+CIPCLOSE=0", 5, "OK");
    Serial.println(count);
    delay(1000);
  }
  else if (value >= 24)
  {
    decibelsValueLoud += log10(value / dBAnalogLoud);
    printDecibels("Loud", decibelsValueLoud);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Please Be Quiet");
    lcd.setCursor(0, 1);
    lcd.print(decibelsValueLoud);
    //delay (1000);
    leds[0] = CRGB::Red;
    FastLED.show();
    //delay(1000);
    //leds[0] = CRGB::Black;
    //FastLED.show();
    String Link = "GET /update?api_key=" + API  + "&field3=" ;
    Link = Link + decibelsValueLoud ;
    Link = Link + " HTTP/1.1\r\n" + "Host: " + HOST + "\r\n" + "Connection: close\r\n\r\n";
    //String getData = "GET /update?api_key=" + API + "&" + field + "=" + String(decibelsValueLoud);
    sendCommand("AT+CIPMUX=1", 5, "OK");
    sendCommand("AT+CIPSTART=0,\"TCP\",\"" + HOST + "\"," + PORT, 15, "OK");
    sendCommand("AT+CIPSEND=0," + String(Link.length() + 4), 4, ">");
    esp8266.println(Link);
    delay(1500);
    countTrueCommand++;
    sendCommand("AT+CIPCLOSE=0", 5, "OK");
    Serial.println(count);
    delay(1000);

  }
  //delay(1000);




}


void printDecibels (const char* volume, float dBs) {
  Serial.print(volume);  Serial.println(dBs);
}
void sendCommand(String command, int maxTime, char readReplay[]) {
  Serial.print(countTrueCommand);
  Serial.print(". at command => ");
  Serial.print(command);
  Serial.print(" ");
  while (countTimeCommand < (maxTime * 1))
  {
    esp8266.println(command);//at+cipsend
    if (esp8266.find(readReplay)) //ok
    {
      found = true;
      break;
    }

    countTimeCommand++;
  }

  if (found == true)
  {
    Serial.println("OK");
    countTrueCommand++;
    countTimeCommand = 0;
  }

  if (found == false)
  {
    Serial.println("Fail");
    countTrueCommand = 0;
    countTimeCommand = 0;
  }

  found = false;
}

