/* Prototype Name : Buzzer Tap with Wifi
   Input Sensor   : Flow Sensor,
   Output Device  : Buzzer, WIFI module
   Libraries Used : LiquidCrystalPCF584, FastLED, SoftwareSerial
*/

#include <LiquidCrystal_PCF8574.h>
LiquidCrystal_PCF8574 lcd(0x3F);
#include <SoftwareSerial.h>
#include<FastLED.h>
#define DATA_PIN 9 // Digital pin number for LED (Multicolor)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];
#define RX 11
#define TX 10
SoftwareSerial esp8266 (RX, TX);
//Wifi and password
String AP = "xxxxxxxxx";       // CHANGE ME
String PASS = "xxxxxxxx"; // CHANGE ME
// API key and thingspeak link , port, field
String API = "OE3OTESYER9SO7NF";   // CHANGE ME
String HOST = "139.59.69.168";
String PORT = "3000";
String field = "field1"; // Modify based on the field in the channel
int count = 0;

int countTrueCommand;
int countTimeCommand;
boolean found = false;
int buzzer = 8; //digital pin 8
int pulsecount = 0;;
byte sensorInterrupt = 0;
byte sensorPin       = 2; //digital pin 2
float calibrationFactor = 4.5;

volatile byte pulseCount;

float flowRate;
unsigned int flowMilliLitres;
unsigned long totalMilliLitres;

unsigned long oldTime;


void setup() {
  Serial.begin(9600);
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
  esp8266.begin(115200);
  sendCommand("AT", 5, "OK");
  sendCommand("AT+CWMODE=1", 5, "OK");
  sendCommand("AT+CWJAP=\"" + AP + "\",\"" + PASS + "\"", 20, "OK");
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  pinMode(buzzer, OUTPUT);
  pinMode(sensorPin, INPUT);
  digitalWrite(sensorPin, HIGH);
  lcd.begin(16, 2);
  lcd.setBacklight(255);
  lcd.setCursor(0, 0);
  lcd.print("Buzzer Tap");
  lcd.clear();
  pulseCount        = 0;
  flowRate          = 0.0;
  flowMilliLitres   = 0;
  totalMilliLitres  = 0;
  oldTime           = 0;
  attachInterrupt(sensorInterrupt, pulseCounter, FALLING);
}

void loop() {
  flow_sensor();
  flowrate_indicator();
  String Link = "GET /update?api_key=" + API  + "&field1=" ;
  Link = Link +  flowRate;
  Link = Link + " HTTP/1.1\r\n" + "Host: " + HOST + "\r\n" + "Connection: close\r\n\r\n";
  delay(1000);  //Read Web Page every 1 seconds
  sendCommand("AT+CIPMUX=1", 5, "OK");
  sendCommand("AT+CIPSTART=0,\"TCP\",\"" + HOST + "\"," + PORT, 15, "OK");
  sendCommand("AT+CIPSEND=0," + String(Link.length() + 4), 4, ">");
  esp8266.println(Link); delay(1500); countTrueCommand++;
  sendCommand("AT+CIPCLOSE=0", 5, "OK");

}
void flow_sensor() {
  if ((millis() - oldTime) > 1000)   // Only process counters once per second
  {

    detachInterrupt(sensorInterrupt);
    flowRate = ((1000.0 / (millis() - oldTime)) * pulseCount) / calibrationFactor;
    oldTime = millis();
    flowMilliLitres = (flowRate / 60) * 1000;
    totalMilliLitres += flowMilliLitres;
    unsigned int frac;
    Serial.print("Flow rate: ");
    Serial.print(int(flowRate));  // Print the integer part of the variabley
    Serial.print(".");             // Print the decimal point
    // Determine the fractional part. The 10 multiplier gives us 1 decimal place.
    frac = (flowRate - int(flowRate)) * 10;
    Serial.print(frac, DEC) ;      // Print the fractional part of the variable
    Serial.print("L/min");
    // Print the number of litres flowed in this second
    Serial.print("  Current Liquid Flowing: ");             // Output separator
    Serial.print(flowMilliLitres);
    Serial.print("mL/Sec");

    // Print the cumulative total of litres flowed since starting
    Serial.print("  Output Liquid Quantity: ");             // Output separator
    Serial.print(totalMilliLitres);
    Serial.println("mL");
    pulseCount = 0;
    attachInterrupt(sensorInterrupt, pulseCounter, FALLING);
  }
}
void flowrate_indicator() {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Flow Rate:");
  lcd.print(int(flowRate));
  if (flowRate > 0 && flowRate <= 2) {
    tone(buzzer, 1000, 500) ;//beeps at frequency of 1000Hz
  }
  else if (flowRate > 2 && flowRate < 5) {
    tone(buzzer, 10000, 500) ;//beeps at frequency of 10000Hz
  }
  else if (totalMilliLitres >= 5) {
    tone(buzzer, 45000, 500) ;//beeps at frequency of 45000Hz
  }

}
void pulseCounter()
{
  // Increment the pulse counter
  pulseCount++;
}
void sendCommand(String command, int maxTime, char readReplay[]) {
  Serial.print(countTrueCommand);
  Serial.print(". at command => ");
  Serial.print(command);
  Serial.print(" ");
  while (countTimeCommand < (maxTime * 1))
  {
    esp8266.println(command);//at+cipsend
    if (esp8266.find(readReplay)) //ok
    {
      found = true;
      break;
    }

    countTimeCommand++;
  }

  if (found == true)
  {
    Serial.println("OK");
    countTrueCommand++;
    countTimeCommand = 0;
  }

  if (found == false)
  {
    Serial.println("Fail");
    countTrueCommand = 0;
    countTimeCommand = 0;
  }

  found = false;
}

