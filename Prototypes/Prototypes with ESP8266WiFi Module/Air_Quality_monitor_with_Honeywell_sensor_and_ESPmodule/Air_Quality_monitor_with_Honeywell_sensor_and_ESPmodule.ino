/* Prototype Name : Know the air Qaulity Levels
   Input Sensor   : honeywell dust sensor, ESP8266 Wifi module(In built in SNT kit)
   Output Device  : Multicolor LED, LiquidCrystalPCF584
   Libraries Used : HPMA115S0, Software Serial, LiquidCrystalPCF584
   FastLED to turn off from use
*/

#include <hpma115S0.h>
#include <SoftwareSerial.h>
//Create an instance of software serial
SoftwareSerial hpmaSerial(5, 6); // Feather TX, Feather RX
//Create an instance of the hpma115S0 library
HPMA115S0 hpma115S0(hpmaSerial);
//For ESP module: 
#define RX 11
#define TX 10
SoftwareSerial esp8266 (RX, TX);

#include <LiquidCrystal_PCF8574.h>
LiquidCrystal_PCF8574 lcd(0x27); // Change the address accordingly
#include<FastLED.h>
#define DATA_PIN 9 // Digital pin number for LED (Multicolor)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];

//Wifi and password
String AP = "Reap_Benefit_2.4";       // CHANGE ME
String PASS = "solvesmalldentbig"; // CHANGE ME
// API key and thingspeak link , port, field
String API = "5X8JKCV7OVKN1DYA";   // CHANGE ME
String HOST = "139.59.69.168";
String PORT = "3000";
String field = "field2"; // Modify based on the field in the channel
int count = 0;

int countTrueCommand;
int countTimeCommand;
boolean found = false;
unsigned int pm2_5, pm10;

void setup() {
  Serial.begin(9600);
  hpmaSerial.begin(9600);
  delay(5000);
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
  Serial.println("Starting...");
  dht.begin();
  lcd.begin(16, 2);
  lcd.setBacklight(255);
  lcd.print("Know Air Quality:");
  delay(2000);
  hpma115S0.Init();
  hpma115S0.StartParticleMeasurement();
  esp8266.begin(115200);
  sendCommand("AT", 5, "OK");
  sendCommand("AT+CWMODE=1", 5, "OK");
  sendCommand("AT+CWJAP=\"" + AP + "\",\"" + PASS + "\"", 20, "OK");


}

void loop() {
  hpmaSerial.listen();
  hpm_serial();
  esp8266.listen();
  hpm_lcd();
  rgb();
  String Link = "GET /update?api_key=" + API  + "&field2=" ;
  Link = Link + pm2_5 ;
  Link = Link + " HTTP/1.1\r\n" + "Host: " + HOST + "\r\n" + "Connection: close\r\n\r\n";
  delay(50000);  //Read Web Page every 50 seconds
  sendCommand("AT+CIPMUX=1", 5, "OK");
  sendCommand("AT+CIPSTART=0,\"TCP\",\"" + HOST + "\"," + PORT, 15, "OK");
  sendCommand("AT+CIPSEND=0," + String(Link.length() + 4), 4, ">");
  esp8266.println(Link); delay(1500); countTrueCommand++;
  sendCommand("AT+CIPCLOSE=0", 5, "OK");


}
void hpm_serial() {

  if (hpma115S0.ReadParticleMeasurement(&pm2_5, &pm10)) {
    Serial.println("PM 2.5: " + String(pm2_5) + " ug/m3" );
    Serial.println("PM 10: " + String(pm10) + " ug/m3" );
  }
  delay(1000);
}
void sendCommand(String command, int maxTime, char readReplay[]) {
  Serial.print(countTrueCommand);
  Serial.print(". at command => ");
  Serial.print(command);
  Serial.print(" ");
  while (countTimeCommand < (maxTime * 1))
  {
    esp8266.println(command);//at+cipsend
    if (esp8266.find(readReplay)) //ok
    {
      found = true;
      break;
    }

    countTimeCommand++;
  }

  if (found == true)
  {
    Serial.println("OK");
    countTrueCommand++;
    countTimeCommand = 0;
  }

  if (found == false)
  {
    Serial.println("Fail");
    countTrueCommand = 0;
    countTimeCommand = 0;
  }

  found = false;
}

void hpm_lcd() {
  lcd.clear();
  lcd.setBacklight(255);
  lcd.setCursor(0, 0);
  lcd.print("Know Air Quality");
  lcd.setCursor(0, 1);
  lcd.print("PM2.5:");
  lcd.print(String(pm2_5));
  lcd.print(" ug/m3");
  if (pm2_5 >= 0 && pm2_5 <= 75) {
    lcd.setCursor(10, 1);
    lcd.print("Good.. ");
    delay(1000);
  } else if (pm2_5 >= 76 && pm2_5 <= 1000) {
    lcd.setCursor(10, 1);
    lcd.print("Bad....");
    delay(1000);
  }
}
void rgb() {
  if (pm2_5 <= 75) {
    leds[0] = CRGB::Green;
    FastLED.show();
    delay(1000);
  }
  else if (pm2_5 >= 76 && pm2_5 <= 1000) {
    leds[0] = CRGB::Red;
    FastLED.show();
    delay(1000);
  }
}

