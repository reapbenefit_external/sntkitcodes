#include <SoftwareSerial.h>
#include <LiquidCrystal_PCF8574.h>

LiquidCrystal_PCF8574 lcd(0x27);

#define RX 11
#define TX 10

SoftwareSerial esp8266 (RX, TX);

String AP = "Reap";       // CHANGE ME
String PASS = "solvesmalldentbig"; // CHANGE ME

String API = "8UO7ZRX8H8ZXIJRV";   // CHANGE ME
String HOST = "api.thingspeak.com";
String PORT = "80";
String field = "field1";
String field2 = "field2";

int count = 0;

int countTrueCommand;
int countTimeCommand;
boolean found = false;
int valSensor = 1;

int trigPin = 5;    // Trigger
int echoPin = 6;    // Echo
long duration, cm, inches;
int full = 0;

void setup() {
  Serial.begin(115200);
  esp8266.begin(115200);
  sendCommand("AT", 5, "OK");
  sendCommand("AT+CWMODE=1", 5, "OK");
  sendCommand("AT+CWJAP=\"" + AP + "\",\"" + PASS + "\"", 20, "OK");
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  lcd.begin(16, 2);
  lcd.setBacklight(255);
  lcd.home(); lcd.clear();
  lcd.print("Smart Bin");
  delay(1000);
  getSensorData();
}

void loop() {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Dist 2 Full: ");
  lcd.print(cm);


  //valSensor = getSensorData();
  getSensorData();
  
  String getData = "GET /update?api_key=" + API + "&" + field + "=" + String(cm) + "&" + field2 + "=" + String(full) ;

  sendCommand("AT+CIPMUX=1", 5, "OK");
  sendCommand("AT+CIPSTART=0,\"TCP\",\"" + HOST + "\"," + PORT, 15, "OK");
  sendCommand("AT+CIPSEND=0," + String(getData.length() + 4), 4, ">");
  esp8266.println(getData); delay(1500); countTrueCommand++;
  sendCommand("AT+CIPCLOSE=0", 5, "OK");
  //Serial.println(count);
  delay(30000);
}

int getSensorData() {

  //  count=(count+1); // Replace with
  //  return count;
  digitalWrite(trigPin, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);

  // Read the signal from the sensor: a HIGH pulse whose
  // duration is the time (in microseconds) from the sending
  // of the ping to the reception of its echo off of an object.
  pinMode(echoPin, INPUT);
  duration = pulseIn(echoPin, HIGH);

  // Convert the time into a distance
  cm = (duration / 2) / 29.1;   // Divide by 29.1 or multiply by 0.0343
  inches = (duration / 2) / 74; // Divide by 74 or multiply by 0.0135

  Serial.print(inches);
  Serial.print("in, ");
  Serial.print(cm);
  Serial.print("cm");
  Serial.println();
  if (cm < 15) {
    full = 1;
    lcd.setCursor(0, 1);
    lcd.print("Help asked: ");
    lcd.print("Yes");
  }
  else {
    full = 0;
    lcd.setCursor(0, 1);
    lcd.print("Help asked: ");
    lcd.print("No");
  }
  //return cm;
  //return full;
  delay(250);
}

void sendCommand(String command, int maxTime, char readReplay[]) {
  Serial.print(countTrueCommand);
  Serial.print(". at command => ");
  Serial.print(command);
  Serial.print(" ");
  while (countTimeCommand < (maxTime * 1))
  {
    esp8266.println(command);//at+cipsend
    if (esp8266.find(readReplay)) //ok
    {
      found = true;
      break;
    }

    countTimeCommand++;
  }

  if (found == true)
  {
    Serial.println("OYI");
    countTrueCommand++;
    countTimeCommand = 0;
  }

  if (found == false)
  {
    Serial.println("Fail");
    countTrueCommand = 0;
    countTimeCommand = 0;
  }

  found = false;
}
