/* Prototype Name : Test Message
   Output Device  : SIM800C Module with SIM card
   Libraries Used : Adafruit FONA Library
   LiquidCrystalPCF584 , FastLED to turn off from use
*/

#include <Wire.h>
#include <LiquidCrystal_PCF8574.h>

LiquidCrystal_PCF8574 lcd(0x3F); // change the address based on the LCD
#include<FastLED.h>
#define DATA_PIN 9 // Digital pin number for LED (Multicolor)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];
#include <Adafruit_FONA.h>
#include <SoftwareSerial.h>

#define FONA_RX 3
#define FONA_TX 7
#define FONA_RST 2
SoftwareSerial fona (FONA_TX, FONA_RX);
Adafruit_FONA fona1 = Adafruit_FONA(FONA_RST);
char replybuffer[255];

void setup() {
  Serial.begin(9600);
  fona.begin(9600);
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
  lcd.begin(16, 2);
  lcd.setBacklight(255);
  lcd.setCursor(0,0);
  lcd.print("SMS based Smart Bin");
}

void loop() {
  fona.print("AT+CMGF=1\r");
  delay(1000);
  fona.print("AT+CMGS=\"+91xxxxxxxxxx\"\r");// enter your phone number

  delay(1000);
  fona.print("Hi!!!");
  fona.print("\r");

  delay(1000);
  fona.println((char)26);
  fona.println();

  delay(10000);
}

