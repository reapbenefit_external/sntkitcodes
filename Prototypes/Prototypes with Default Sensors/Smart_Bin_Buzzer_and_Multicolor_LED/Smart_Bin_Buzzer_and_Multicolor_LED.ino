/* Prototype Name : Smart Bin
   Input Sensor   : Distance Sensor
   Output Device  : LCD, Buzzer
   Libraries Used : LiquidCrystalPCF584 and FastLED
   NewPing Library is not used here
*/

#include <LiquidCrystal_PCF8574.h> // https://github.com/mathertel/LiquidCrystal_PCF8574
LiquidCrystal_PCF8574 lcd(0x27); // Check the sticker on the LCD to know the address
#include<FastLED.h>
#define DATA_PIN 9 // Digital pin number for LED (Multicolor)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];

#define trigPin 5
#define echoPin 6
#define buzzer 9

int distance;

void setup() {
  Serial.begin (9600);
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
  pinMode(trigPin, OUTPUT);
  pinMode(buzzer, OUTPUT);
  pinMode(echoPin, INPUT);
  lcd.begin(16, 2);
  lcd.setBacklight(255);
  lcd.home();
  lcd.clear();
  lcd.print("Hello");
  delay(1000);
  lcd.clear();
}

void loop() {
  int timetaken, dist;
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(1000);
  digitalWrite(trigPin, LOW);
  timetaken = pulseIn(echoPin, HIGH);
  distance = (timetaken / 2) * 0.034049 ;
  if (distance >= 0 && distance <= 7) {
    lcd.setCursor(0, 0);
    lcd.print("Bin is full");
    digitalWrite(buzzer, HIGH);
    leds[0] = CRGB::Red;
    FastLED.show();
    delay(3000);
  }
  else {
    lcd.setCursor(0, 0);
    lcd.print("Bin Not full");
    digitalWrite(buzzer, LOW);
    leds[0] = CRGB::Green;
    FastLED.show();
    delay(3000);
  }
  delay(500);
}
