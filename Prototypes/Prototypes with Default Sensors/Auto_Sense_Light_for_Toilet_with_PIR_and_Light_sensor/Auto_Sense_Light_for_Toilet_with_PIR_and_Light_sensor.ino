/* Prototype Name : Auto Sense Light
   Input Sensor   : Light Sensor, Motion Sensor
   Output Device  : Relay
   Libraries Used : LiquidCrystalPCF584 and FastLED to turn off from use. 
*/
#include <LiquidCrystal_PCF8574.h>
LiquidCrystal_PCF8574 lcd(0x27); // change to 0X3F if necessary
#include <FastLED.h> //

#define DATA_PIN 9 // Digital pin number for LED (NeoPixel)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];

int ldr_pin = 1; //Analog pin A1
int relay_pin = 4 ;// Digital pin 4 for the RELAY
int pir_pin = 3;   // Digital pin 3(for PIR sensor)

void setup() {
  Serial.begin(9600);
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
  pinMode(relay_pin, OUTPUT); // declare lamp as output
  pinMode(pir_pin, INPUT); // declare sensor as input
}
void loop() {
  int value_ldr = analogRead(ldr_pin); // read LDR value
  int value_pir = digitalRead(pir_pin); // read input value
  Serial.println(value_ldr);
  Serial.println(value_pir);

  if ((value_ldr < 700) && ( value_pir == HIGH) ) {
    digitalWrite(relay_pin, HIGH); // Turn ON the light
    delay(6000);
  }
  else {
    digitalWrite(relay_pin, LOW); // Turn OFF the light
  }

}

