/* Prototype Name : Automatic Light 
   Input Sensor   : LDR
   Output Device  : Relay, LCD
   Libraries Used : FastLED to turn off from use
*/

#include <LiquidCrystal_PCF8574.h>
LiquidCrystal_PCF8574 lcd(0x3f); //change to 0x27 if need be
#include<FastLED.h>

int sensorPin = 1;   // Abalog input pin A1 for ldr
int sensorValue = 0;  // variable to store the value coming from the sensor
int relay_pin = 4 ; // Relay digital pin D4

#define DATA_PIN 9 // Digital pin number for LED (Multicolor)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];

void setup() {
  pinMode(relay_pin, OUTPUT); //pin connected to the relay
  Serial.begin(9600); //sets serial port for communicationSerial.begin(9600);
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
  lcd.begin(16, 2);
  lcd.setBacklight(255);
  lcd.setCursor(0, 0);
  lcd.print("Automatic light");
  delay(2000);
  
}

void loop() {
  // read the value from the sensor:
  sensorValue = analogRead(sensorPin);
  Serial.println(sensorValue); //prints the values coming from the sensor on the screen
  delay(1000);
  if (sensorValue < 700) { //setting a threshold value for LDR
    digitalWrite(7, LOW); //turn relay ON
    delay(1000); // Wait for a second
  }
  else {
    digitalWrite(7, HIGH); //turn relay OFF
    delay(1000);
  }
}
