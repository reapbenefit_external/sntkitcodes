/*Student idea of Detecting Block in Drainages with
Distance sensor and Buzzer
displays values in Serial Monitor
Uses NewPing Library*/

#include <NewPing.h>
#define TRIGGER_PIN  5  // Arduino pin tied to trigger pin on the ultrasonic sensor.
#define ECHO_PIN     6 // Arduino pin tied to echo pin on the ultrasonic sensor.
#define MAX_DISTANCE 200 // Maximum distance we want to ping for (in centimeters). Maximum sensor distance is rated at 400-500cm.
int distance;
int buzzer = 8;

NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // NewPing setup of pins and maximum distance.

void setup() {
  Serial.begin(9600); // Open serial monitor at 9600 baud to see ping results.
  pinMode(buzzer, OUTPUT);
}
void loop() {
  Serial.print("Ping: ");
  distance = (sonar.ping_cm()); // Send ping, get distance in cm and print result (0 = outside set distance range)
  Serial.println("cm");
  if (distance <= 10)
  {
    Serial.println (" Drain Blocked ");
    Serial.print (" Distance= ");
    Serial.println (distance);
    digitalWrite (buzzer, HIGH);
    delay (1000);
  }
  else {
    Serial.println (" Drain Clear ");
    Serial.print (" Distance= ");
    Serial.println (distance);
    digitalWrite (buzzer, LOW);
    delay (1000);
  }
}


