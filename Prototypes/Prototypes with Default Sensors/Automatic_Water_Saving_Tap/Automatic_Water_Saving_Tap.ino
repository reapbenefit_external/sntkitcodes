/* Prototype Name : Automatic Water Saving Tap
   Input Sensor   : Distance Sensor
   Output Device  : Relay
   Libraries Used : NewPing
   LiquidCrystalPCF584 ,FastLED to turn off from use
*/

#include <NewPing.h>
#include <Wire.h>
#include <LiquidCrystal_PCF8574.h>
LiquidCrystal_PCF8574 lcd(0x3F);
#include <FastLED.h>
#define DATA_PIN 9// Digital pin number for LED (NeoPixel)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];
#define TRIGGER_PIN  5 // Arduino pin tied to trigger pin on the ultrasonic sensor.
#define ECHO_PIN     6 // Arduino pin tied to echo pin on the ultrasonic sensor.
#define MAX_DISTANCE 200 // Maximum distance we want to ping for (in centimeters). Maximum sensor distance is rated at 400-500cm.

NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // NewPing setup of pins and maximum distance.
int relay = 4;
void setup() {
  Serial.begin(9600); // Open serial monitor at 115200 baud to see ping results.
  pinMode(relay, OUTPUT);
  lcd.begin(16, 2);
  lcd.setCursor(0, 0);
  lcd.print("Auto Tap");
}

void loop() {
  delay(500);                     // Wait 50ms between pings (about 20 pings/sec). 29ms should be the shortest delay between pings.
  Serial.print("Ping: ");
  int distance = sonar.ping_cm();
  Serial.print(distance); // Send ping, get distance in cm and print result (0 = outside set distance range)
  Serial.println("cm");
  if (distance >= 0 && distance <= 10) {
    digitalWrite(relay, HIGH);
    delay(1000);
  }
  else {
    digitalWrite(relay, LOW);
    delay(1000);
  }

}

