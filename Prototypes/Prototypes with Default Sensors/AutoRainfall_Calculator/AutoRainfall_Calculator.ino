/* Prototype Name : Auto Rainfall Water Calculator
   Input Sensor   : Distance Sensor
   Output Device  : LCD
   Libraries Used : LiquidCrystalPCF584
   FastLED to turn off from use
   Refer to the formula used here
   rainrecievedlits = ((lengthofroof * breadthofroof * runoffcoeff) * (distancefromsensortofloor - dist) * 0.01);
*/
#include <LiquidCrystal_PCF8574.h>
LiquidCrystal_PCF8574 lcd(0x3F); //change to 0x27 if necessary
#include <FastLED.h>

#define DATA_PIN 9// Digital pin number for LED (NeoPixel)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];

#define trigPin 5
#define echoPin 6
//change based on the measurement of the roof
float lengthofroof = 22.2; //in centimeters
float breadthofroof = 20.5; //in centimeters
float runoffcoeff = 0.8;
float distancefromsensortofloor = 30.0; // in centimeters
int rainrecievedlits;
int timetaken, dist;

void setup() {
  Serial.begin (9600);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  lcd.begin(16, 2); // initialize the lcd
  lcd.setBacklight(255);
  lcd.home();
  lcd.clear();
  lcd.print("Rainfall Calculator:");
  delay(1000);
}

void loop() {
  delay(2000);
  rainfall(); 
  showlcd();
}
void rainfall() {
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(1000);
  digitalWrite(trigPin, LOW);
  timetaken = pulseIn(echoPin, HIGH);
  dist = (timetaken / 2) * 0.034049 ;
  Serial.println("Distance in CM: ");
  Serial.println(dist);
  delay(500);

  rainrecievedlits = ((lengthofroof * breadthofroof * runoffcoeff) * (distancefromsensortofloor - dist) * 0.01);
  Serial.println(rainrecievedlits);
}
void showlcd() {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Dist(cm): ");
  lcd.print(dist);
  lcd.setCursor(0, 1);
  lcd.print("Rain (Lits) now: ");
  lcd.print(rainrecievedlits);
}
