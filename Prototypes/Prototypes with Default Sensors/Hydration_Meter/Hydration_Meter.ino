/* Prototype Name : Hydration Meter
   Input Sensor   : Moisture Sensor
   Output Device  : LCD, Multicolor LED
   Libraries Used : LiquidCrystalPCF584 ,FastLED
*/
#include <LiquidCrystal_PCF8574.h>
LiquidCrystal_PCF8574 lcd(0x3F); //Change accordingly
#include <FastLED.h>
#define DATA_PIN 9// Digital pin number for LED (NeoPixel)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];
int sensor_pin = A0;

void setup()
{
  Serial.begin(9600);
  delay(2000);
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
  lcd.begin(16, 2);
  lcd.setBacklight(255);
  lcd.home(); lcd.clear();
  lcd.print("Hydration Meter");
  delay(1000);
  lcd.clear();
}

void loop()
{
  int output = analogRead(sensor_pin);
  lcd.setCursor(0, 0);
  lcd.print("Val:");
  lcd.setCursor(9, 0);
  lcd.println(output);
  if (output > 0 && output <= 400) // Moisture is Wet
  {

    leds[0] = CRGB::Red;
    FastLED.show();
    lcd.setCursor(0, 1);
    lcd.print("U're hydrated :-)");

  }
  else
  {

    leds[0] = CRGB::Red;
    FastLED.show();
    lcd.setCursor(0, 1);
    lcd.print("Drink Water");
  }
}
