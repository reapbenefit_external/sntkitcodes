/* Prototype Name : Lung Capacity Doctor
   Input Sensor   : Flow Sensor
   Output Device  : LCD
   Libraries Used : LiquidCrystalPCF584 , FastLED
*/

#include <Wire.h>
#include <LiquidCrystal_PCF8574.h>
LiquidCrystal_PCF8574 lcd(0x27);
#include <FastLED.h>
#define DATA_PIN 9// Digital pin number for LED (NeoPixel)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];

byte sensorInterrupt = 0;  // 0 = digital pin 2
byte sensorPin       = 2;
float calibrationFactor = 4.5;

volatile byte pulseCount;

float flowRate;
unsigned int flowMilliLitres;
unsigned long totalMilliLitres;

unsigned long oldTime;

void setup()
{

  // Initialize a serial connection for reporting values to the host
  Serial.begin(115200);
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
  pinMode(sensorPin, INPUT);
  digitalWrite(sensorPin, HIGH);

  pulseCount        = 0;
  flowRate          = 0.0;
  flowMilliLitres   = 0;
  totalMilliLitres  = 0;
  oldTime           = 0;
  attachInterrupt(sensorInterrupt, pulseCounter, FALLING);

  lcd.begin(16, 2);
  lcd.setBacklight(255);
  lcd.home(); lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Hi, Know Your ");
  lcd.setCursor(0, 1);
  lcd.print("Lung Capacity ");
  delay(4000);

}

/**
   Main program loop
*/
void loop()
{

  if ((millis() - oldTime) > 1000)   // Only process counters once per second
  {

    detachInterrupt(sensorInterrupt);
    flowRate = ((1000.0 / (millis() - oldTime)) * pulseCount) / calibrationFactor;
    oldTime = millis();
    flowMilliLitres = (flowRate / 60) * 1000;
    totalMilliLitres += flowMilliLitres;

    unsigned int frac;

    // Print the flow rate for this second in litres / minute
    Serial.print("Flow rate: ");
    Serial.print(int(flowRate));  // Print the integer part of the variable
    Serial.print(".");             // Print the decimal point
    // Determine the fractional part. The 10 multiplier gives us 1 decimal place.
    frac = (flowRate - int(flowRate)) * 10;
    Serial.print(frac, DEC) ;      // Print the fractional part of the variable
    Serial.print("L/min");
    // Print the number of litres flowed in this second
    Serial.print("  Current Liquid Flowing: ");             // Output separator
    //lcd.setCursor(0, 0);
    //lcd.print(flowMilliLitres);
    Serial.print(flowMilliLitres);
    Serial.print("mL/Sec");

    // Print the cumulative total of litres flowed since starting
    Serial.print("  Output Liquid Quantity: ");             // Output separator
    //lcd.setCursor(0, 1);
    //lcd.print(totalMilliLitres);
    Serial.print(totalMilliLitres);
    Serial.println("mL");

    // Reset the pulse counter so we can start incrementing again
    pulseCount = 0;

    // Enable the interrupt again now that we've finished sending output
    attachInterrupt(sensorInterrupt, pulseCounter, FALLING);

    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("LungCap(mL)");
    lcd.print(totalMilliLitres);
    lcd.setCursor(0, 1);

    if (totalMilliLitres >= 0 && totalMilliLitres <= 500) {
      lcd.print("Average.");
      Serial.print("See a doctor");
      leds[0] = CRGB::Red;
      FastLED.show();
    }
    else if (totalMilliLitres >= 501 && totalMilliLitres <= 3000) {
      lcd.print("Good");
      Serial.print("Good");
      leds[0] = CRGB::Green;
      FastLED.show();
    }
    else {
      lcd.print("Excellent");
      Serial.print("Excellent");
      leds[0] = CRGB::Blue;
      FastLED.show();
      lcd.home();
    }
  }
}

/*
  Insterrupt Service Routine
*/
void pulseCounter()
{
  // Increment the pulse counter
  pulseCount++;
}


