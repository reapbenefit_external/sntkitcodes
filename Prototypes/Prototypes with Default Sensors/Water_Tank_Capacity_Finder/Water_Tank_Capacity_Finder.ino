/* Prototype Name : Water Tank Capacity Finder
   Input Sensor   : Distance sensor
   Output Device  : LCD
   Libraries Used : LiquidCrystalPCF584
   FastLED to turn off from use
*/

#include <LiquidCrystal_PCF8574.h>
LiquidCrystal_PCF8574 lcd(0x27);
#define DATA_PIN 9  // Digital pin number for LED (NeoPixel)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];

#define trigPin 5
#define echoPin 6
// Change accordingly to the dimensions of the tank (in cm)
int lengthoftank = 18;
int breadthoftank = 14;
int distfromfloor = 10;

void setup() {
  Serial.begin (9600);
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(buzzer, OUTPUT);
  lcd.begin(16, 2);
  lcd.setBacklight(255);
  lcd.home(); lcd.clear();
  lcd.print("Tank Capacity");
  delay(1000);
  lcd.clear();
}

void loop() {
  int timetaken, dist;
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(1000);
  digitalWrite(trigPin, LOW);
  timetaken = pulseIn(echoPin, HIGH);
  dist = (timetaken / 2) * 0.034049 ;
  if (dist >= 500 || dist <= 0) {
    Serial.println("Out Of Range");
  }
  else {
    Serial.println("Distance in CM: ");
    Serial.print(dist);
  }
  delay(500);

  tankcapacity = (lengthoftank * breadthoftank * (distfromfloor - dist));
  Serial.println(tankcapacity);
  lcd.setCursor(0, 0);
  lcd.print("Tank Capacity(lt)");
  lcd.setCursor(0, 1);
  lcd.print(tankcapacity);

}

