/* Prototype Name : AutoWatering Plant
   Input Sensor   : Moisture Sensor
   Output Device  : LCD, Multicolor LED, Relay connected to Water pump
   Libraries Used : LiquidCrystalPCF584 ,FastLED
*/
#include <LiquidCrystal_PCF8574.h>
LiquidCrystal_PCF8574 lcd(0x3F); //Change accordingly
#include <FastLED.h>
#define DATA_PIN 9// Digital pin number for LED (NeoPixel)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];
int sensor_pin = A0;
#define Relay 4

void setup()
{
  Serial.begin(9600);
  pinMode(Relay, OUTPUT);
  delay(2000);
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
  lcd.begin(16, 2);
  lcd.setBacklight(255);
  lcd.home(); lcd.clear();
  lcd.print("AutoWatering Plant");
  delay(1000);
  lcd.clear();
}

void loop() {
  int output = analogRead(sensor_pin);
  lcd.setCursor(0, 0);
  lcd.print("Moisture = ");
  lcd.setCursor(0, 1);
  lcd.println(output);
  if (output > 0 && output <= 400) // Moisture is Wet
  {
    digitalWrite(Relay, LOW); // Pump Off
    leds[0] = CRGB::Green;
    FastLED.show();

  }
  else
  {
    digitalWrite(Relay, HIGH); // Pump On
    leds[0] = CRGB::Red;
    FastLED.show();
    delay(1000);
  }
}
