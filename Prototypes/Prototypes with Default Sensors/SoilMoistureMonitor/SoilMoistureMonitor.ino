/* Prototype Name : Soil Moisture Monitor
   Input Sensor   : Moisture sensor
   Output Device  : Buzzer, Multicolor LED
   Libraries Used : LiquidCrystalPCF584 ,FastLED to turn off from use
*/
#include <LiquidCrystal_PCF8574.h> // https://github.com/mathertel/LiquidCrystal_PCF8574
LiquidCrystal_PCF8574 lcd(0x3F); // Check the sticker on the LCD to know the address

#include <FastLED.h> //
#define DATA_PIN 9// Digital pin number for LED (NeoPixel)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];
int soil_pin = 0;
int buzzer = 8;
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  pinMode( buzzer, OUTPUT);
  lcd.begin(16, 2);
  lcd.setBacklight(255);
  lcd.print("Plant Monitor");
  delay(1000);
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  pinMode(4, OUTPUT);
}

// the loop routine runs over and over again forever:
void loop() {
  leds[0] = CRGB::Black;
  FastLED.show();
  // read the input on analog pin 0:
  int sensorValue = analogRead(soil_pin);
  // print out the value you read:
  Serial.println(sensorValue);

  lcd.setCursor(0, 0);
  lcd.print("MositureVal:");
  lcd.print(sensorValue);
  delay(1000);        // delay in between reads for stability
  if (sensorValue > 0 && sensorValue < 420)
  {

    digitalWrite(buzzer, LOW);
    //delay(000); // Beeps if its Salt water.
    lcd.setCursor(0, 1);
    lcd.print("Plant is Watered");
    leds[0] = CRGB::Green;
    FastLED.show();
    digitalWrite(4, HIGH);
    delay(1000);
  }
  if (sensorValue >= 421 && sensorValue <= 1023) {
    digitalWrite(buzzer, HIGH);
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 1);
    lcd.print("Water the Plant");
    leds[0] = CRGB::Red;
    FastLED.show();
    digitalWrite(4, LOW);
    delay(1000);
  }
}
