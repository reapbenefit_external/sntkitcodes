/* Prototype Name : Rainfall Calculator
   Input Sensor   : Distance Sensor
   Output Device  : LCD
   Libraries Used : LiquidCrystalPCF584
   FastLED to turn off from use
*/
#include <LiquidCrystal_PCF8574.h>
LiquidCrystal_PCF8574 lcd(0x3F); //Change accordingly
#include <FastLED.h>
#define DATA_PIN 9// Digital pin number for LED (NeoPixel)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];

#define trigPin 5
#define echoPin 6

//Modify the dimensions accordingly
float lengthofroof = 25.0; //in meters
float breadthofroof = 20.0; //meters
float runoffcoeff = 0.8; // Defualt
float distancefromsensortofloor = 10.1; // Change accordinly where the distance sensor is postioned
int rainrecievedlits;

void setup() {
  Serial.begin (9600);
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  delay(1000);
  lcd.begin(16, 2);
  lcd.setBacklight(255);
  lcd.home(); lcd.clear();
  lcd.print("Rainfall Calculator");
  delay(1000);
  lcd.clear();

}

void loop() {
  int timetaken, dist;
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(1000);
  digitalWrite(trigPin, LOW);
  timetaken = pulseIn(echoPin, HIGH);
  dist = (timetaken / 2) * 0.034049 ;
  if (dist >= 0 && dist <= 200) {
    delay(1000);
    rainrecievedlits = ((lengthofroof * breadthofroof * runoffcoeff) * (distancefromsensortofloor - dist) * 0.01);
    lcd.setCursor(0, 0);
    lcd.print("This Rain(lt)");
    lcd.setCursor(0, 1);
    lcd.print(rainrecievedlits);
  }
}

