/* Prototype Name : Distance Meter
   Input Sensor   : Distance Sensor
   Output Device  : LCD
   Libraries Used : LiquidCrystalPCF584, FastLED
*/

#include <LiquidCrystal_PCF8574.h>
LiquidCrystal_PCF8574 lcd(0x3f); //change to 0x27 if need be
#include<FastLED.h>

const int trigPin = 5;
const int echoPin = 6;
#define DATA_PIN 9 // Digital pin number for LED (Multicolor)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];

void setup() {
  Serial.begin(9600);
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
  lcd.begin(16, 2);
  lcd.setBacklight(255);
  lcd.setCursor(0, 0);
  lcd.print("Know Your height");
  delay(2000);
}

void loop()
{
  long duration, inches, cm, mm;
  pinMode(trigPin, OUTPUT);
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2000);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(1000);
  digitalWrite(trigPin, LOW);
  pinMode(echoPin, INPUT);
  duration = pulseIn(echoPin, HIGH);
  inches = microsecondsToInches(duration);
  cm = microsecondsToCentimeters(duration);
  mm = microsecondsToMillimeters(duration);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Height is:");
  lcd.print(cm); // Feel free to modify in inches or mm
  lcd.print("cm, ");
  delay(1000);
}
long microsecondsToInches(long microseconds)
{
  return microseconds / 74 / 2;
}
long microsecondsToCentimeters(long microseconds)
{
  return microseconds / 29 / 2;
}
long microsecondsToMillimeters(long microseconds)
{
  return microseconds / 2.9 / 2;
}
