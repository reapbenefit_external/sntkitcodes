/*Prototype Name : Cleaning Indicator with Light Sensor
   Input Sensor   : Light Sensor
   Output Device  : Buzzer
   Libraries Used : LiquidCrystalPCF584
   FastLED to turn off from use
  Based on values of light intensity, we can identify easily the desk/window/door has to be cleaned or not.
  What happens when Light intensity value is higher? Clean or Unclean?
*/
#define ldr A1 //analog pin A1
#define buzzer 8 //Digital pin 8
int sensorValue;

void setup() {
  Serial.begin(9600);
  pinMode(ldr, INPUT);
  pinMode(buzzer, OUTPUT);
}

void loop() {
  // Wait a few seconds between measurements.
  delay(2000);
  sesnorValue = analogRead(ldr); //analog reading has the range 0-1023
  Serial.println(sesnorValue);
  delay(3000);

}
void clean_test() {
  if (sesnorValue > 0 && sensorValue < 400) {
    digitalWrite(buzzer, HIGH);
    delay(1000);
  }
  else if (sensorValue > 400 && sensorValue < 1024) {
    digitalWrite(buzzer, LOW);
    delay(1000);
  }
}

