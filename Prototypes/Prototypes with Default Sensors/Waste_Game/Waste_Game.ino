/* Prototype Name : Know the Type of Waste
   Input Sensor   : Moisture sensor
   Output Device  : LCD
   Libraries Used : LiquidCrystalPCF584
   FastLED to turn off from use
*/

#define buzzer 8
#include <LiquidCrystal_PCF8574.h>
LiquidCrystal_PCF8574 lcd(0x3F);  // set the LCD address accordingly
#include <FastLED.h>
#define DATA_PIN 9// Digital pin number for LED (NeoPixel)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];

void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  pinMode(buzzer, OUTPUT);
  lcd.begin(16, 2); // initialize the lcd
  lcd.print("Waste Segregation");
  delay(1000);
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
}

void loop() {
  // read the input on analog pin 0:
  int sensorValue = analogRead(A0);
  Serial.println(sensorValue);
  delay(1000);


  if (sensorValue > 0 && sensorValue < 400) {
    Serial.println("Wet Waste");
    digitalWrite(buzzer, HIGH);
    lcd.setBacklight(255);
    lcd.home();
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Wet Waste");

  }
  else {
    Serial.println("Dry Waste");
    digitalWrite(buzzer, LOW);
    lcd.setBacklight(255);
    lcd.home();
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Dry Waste");

  }

}

