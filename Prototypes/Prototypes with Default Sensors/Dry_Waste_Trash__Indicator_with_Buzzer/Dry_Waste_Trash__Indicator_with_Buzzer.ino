/* Prototype Name : Trash Indicator with Buzzer
   Input Sensor   : Distance Sensor
   Output Device  : Buzzer
   Libraries Used : LiquidCrystalPCF584, FastLED to turn off from use
*/
#include <LiquidCrystal_PCF8574.h>
LiquidCrystal_PCF8574 lcd(0x3F); //change the address if necessary
#include<FastLED.h>
#define DATA_PIN 9 // Digital pin number for LED (Multicolor)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];

#define trigPin 5 // digital pin 5
#define echoPin 6 //digital pin 6
#define buzzer 8    //digital pin 8

void setup() {
  Serial.begin (9600);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(buzzer, OUTPUT);
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
  delay(1000);
  lcd.begin(16, 2);
  lcd.setCursor(0, 0);
  lcd.print("Trash Indicator");
  delay(3000);
}

void loop() {
  distance();
  trash_test();
}
void distance() {
  delay(100);
  int timetaken, dist;
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(1000);
  digitalWrite(trigPin, LOW);
  timetaken = pulseIn(echoPin, HIGH);
  dist = (timetaken / 2) * 0.034049 ;
  Serial.print("Distance in CM:");
  Serial.println(dist);
  delay(500);
}
void trash_test() {
  if (distance > 0 && distance < 18) //Change the distance here depending on the dustbin height
  {
    digitalWrite(buzzer, HIGH); //
    delay(500);
  }
  else {
    digitalWrite(buzzer, LOW); //
  }
}

