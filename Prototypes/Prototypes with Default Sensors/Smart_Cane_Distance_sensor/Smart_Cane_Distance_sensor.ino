/* Prototype Name : Smart Cane for Blind people
   Input Sensor   : Distance Sensor
   Output Device  : Buzzer
   Libraries Used : LiquidCrystalPCF584, FastLED to turn off from use
*/
//Libraries
#include <LiquidCrystal_PCF8574.h>
LiquidCrystal_PCF8574 lcd(0x3F); //Else change to 0x3F

//Variables
#define trigPin 5
#define echoPin 6
#define buzzer 8
int timetaken, dist;

//Loop section
void setup() {
  // put your setup code here, to run once:
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
  lcd.begin(16, 2);
  lcd.setBacklight(255);
  lcd.home();
  lcd.print("Smart Cane");
  delay(3000);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(buzzer, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  delay(100);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(1000);
  digitalWrite(trigPin, LOW);
  timetaken = pulseIn(echoPin, HIGH);
  dist = (timetaken / 2) * 0.034049 ;
  delay(500);
  func_cane(); // Call the sub function here.
}

void func_cane() {
  if (dist < 20) // Checking the distance, you can change the value
  {
    digitalWrite(buzzer, HIGH);
  } else {
    digitalWrite(buzzer, LOW);
  } delay(500);
}

