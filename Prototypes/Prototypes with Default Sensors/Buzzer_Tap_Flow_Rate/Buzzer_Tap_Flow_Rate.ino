/* Prototype Name : Buzzer Tap
   Input Sensor   : Flow Sensor,
   Output Device  : Buzzer
   Libraries Used : LiquidCrystalPCF584, FastLED
*/

#include <LiquidCrystal_PCF8574.h>
LiquidCrystal_PCF8574 lcd(0x3F);
#include<FastLED.h>
#define DATA_PIN 9 // Digital pin number for LED (Multicolor)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];

int buzzer = 8; //digital pin 8
int pulsecount = 0;;
byte sensorInterrupt = 0;
byte sensorPin       = 2; //digital pin 2
float calibrationFactor = 4.5;

volatile byte pulseCount;

float flowRate;
unsigned int flowMilliLitres;
unsigned long totalMilliLitres;

unsigned long oldTime;


void setup() {
  Serial.begin(9600);
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
  pinMode(buzzer, OUTPUT);
  pinMode(sensorPin, INPUT);
  digitalWrite(sensorPin, HIGH);
  lcd.begin(16, 2);
  lcd.setBacklight(255);
  lcd.setCursor(0, 0);
  lcd.print("Buzzer Tap");
  lcd.clear();
  pulseCount        = 0;
  flowRate          = 0.0;
  flowMilliLitres   = 0;
  totalMilliLitres  = 0;
  oldTime           = 0;
  attachInterrupt(sensorInterrupt, pulseCounter, FALLING);
}

void loop() {
  flow_sensor();
  flowrate_indicator();

}
void flow_sensor() {
  if ((millis() - oldTime) > 1000)   // Only process counters once per second
  {

    detachInterrupt(sensorInterrupt);
    flowRate = ((1000.0 / (millis() - oldTime)) * pulseCount) / calibrationFactor;
    oldTime = millis();
    flowMilliLitres = (flowRate / 60) * 1000;
    totalMilliLitres += flowMilliLitres;
    unsigned int frac;
    Serial.print("Flow rate: ");
    Serial.print(int(flowRate));  // Print the integer part of the variabley
    Serial.print(".");             // Print the decimal point
    // Determine the fractional part. The 10 multiplier gives us 1 decimal place.
    frac = (flowRate - int(flowRate)) * 10;
    Serial.print(frac, DEC) ;      // Print the fractional part of the variable
    Serial.print("L/min");
    // Print the number of litres flowed in this second
    Serial.print("  Current Liquid Flowing: ");             // Output separator
    Serial.print(flowMilliLitres);
    Serial.print("mL/Sec");

    // Print the cumulative total of litres flowed since starting
    Serial.print("  Output Liquid Quantity: ");             // Output separator
    Serial.print(totalMilliLitres);
    Serial.println("mL");
    pulseCount = 0;
    attachInterrupt(sensorInterrupt, pulseCounter, FALLING);
  }
}
void flowrate_indicator() {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Flow Rate:");
  lcd.print(int(flowRate));
  if (flowRate > 0 && flowRate <= 2) {
    tone(buzzer, 1000, 500) ;//beeps at frequency of 1000Hz
  }
  else if (flowRate > 2 && flowRate < 5) {
    tone(buzzer, 10000, 500) ;//beeps at frequency of 10000Hz
  }
  else if (totalMilliLitres >= 5) {
    tone(buzzer, 45000, 500) ;//beeps at frequency of 45000Hz
  }

}
void pulseCounter()
{
  // Increment the pulse counter
  pulseCount++;
}

