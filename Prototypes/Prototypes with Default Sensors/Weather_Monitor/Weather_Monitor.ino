/* Prototype Name : Weather Monitor
   Input Sensor   : DHT22 Sensor
   Output Device  : LCD, Multicolor LED
   Libraries Used : DHT, LiquidCrystalPCF584, FastLED
*/
#include<DHT.h>
#include<Adafruit_Sensor.h>
#include<FastLED.h>
#include <Wire.h>
#include <LiquidCrystal_PCF8574.h>
LiquidCrystal_PCF8574 lcd(0x3f);


DHT dht (7, DHT22); // 7 denptes Digital pin for DHT22 sensor, DHT22 denotes DHT type
#define DATA_PIN 9 // Digital pin number for LED (Multicolor)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];


void setup() {
  Serial.begin(9600);
  Serial.print("DHT test");
  dht.begin();
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
  delay(1000);
  lcd.begin(16, 2);
  lcd.setBacklight(255);
  lcd.home(); lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Weather ");
  lcd.setCursor(0, 1);
  lcd.print("Monitor");
  delay(4000);
}

void loop() {

  delay(2000);
  float humidity = dht.readHumidity();
  float temperature = dht.readTemperature();

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("T(C):");
  lcd.print(temperature);
  lcd.print("Hum(%)");
  lcd.print(humidity);
  delay(1000); //Wait for a second
  
  if ( temperature >= 0 && temperature <= 24 ) {
    lcd.setCursor(0, 1);
    lcd.print(" It's cold");
  }
  else
  {
    lcd.print(" It's warm");
  }

}
