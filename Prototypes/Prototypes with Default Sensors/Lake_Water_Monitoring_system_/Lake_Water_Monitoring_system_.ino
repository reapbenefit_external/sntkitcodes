/* Prototype Name : Lake Water Monitoring System(Moisture)
   Input Sensor   : Two Moisture sensors Sensor
   Output Device  : Buzzer
   Libraries Used : LiquidCrystalPCF584
    ,FastLED to turn off from use
*/

#include <LiquidCrystal_PCF8574.h>
LiquidCrystal_PCF8574 lcd(0x3F);
#include <FastLED.h>

#define DATA_PIN 9// Digital pin number for LED (NeoPixel)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];

int sensor0 = A0;
int sensor1 = A2;
int threshold = 400;

void setup() {
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
  pinMode(sensor0, INPUT);
  pinMode(sensor1, INPUT);
  Serial.begin(9600);
  lcd.begin(16, 2);
  lcd.setBacklight(255);
  lcd.home();
  lcd.print("Hello ");
  delay(1000);

}
void loop() {
  // read the input on analog pin 0:
  int sensorValue0 = analogRead(sensor0);
  Serial.print("Sensor0");
  Serial.println(sensorValue0);
  delay(1000);

  int sensorValue1 = analogRead(sensor1);
  Serial.println(sensorValue1);
  delay(1000);
  //lcd.setCursor(0, 0);
  if (sensorValue0 > threshold) {
    Serial.println (" Lake is dry ");
    lcd.setCursor(0, 0);
    lcd.print(" Sensor0 is dry ");
    delay(1000);

  }
  else {
    Serial.println (" Lake is Wet ");
    lcd.setCursor(0, 0);
    lcd.print(" Sensor0 is wet ");
    delay(1000);
  }

  if (sensorValue1 > threshold) {
    Serial.println (" Lake is dry ");
    lcd.setCursor(0, 1);
    lcd.print(" Sensor1 is dry ");
    delay(1000);
  }
  else {
    Serial.println (" Lake is Wet ");
    lcd.setCursor(0, 1);
    lcd.print(" Sensor1 is wet ");
    delay(1000);
  }
}
