/* Prototype Name : Water Salinity Tester
   Input Sensor   : Moisture Sensor
   Output Device  : LCD, Multicolor LED
   Libraries Used : LiquidCrystalPCF584 ,FastLED
*/
#include <LiquidCrystal_PCF8574.h>
LiquidCrystal_PCF8574 lcd(0x27); //Change according to address on LCD 
#include <FastLED.h>
#define DATA_PIN 9// Digital pin number for LED (NeoPixel)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];
int sensor_pin = 0;
int output;
void setup()
{
  Serial.begin(9600);
  delay(2000);
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
  lcd.begin(16, 2);
  lcd.setBacklight(255);
  lcd.home(); lcd.clear();
  lcd.print("Water Test");
  delay(1000);

}

void loop()
{
  output = analogRead(sensor_pin);
  delay(500);
  lcd.setCursor(0, 0);
  lcd.print("Val:    ");
  lcd.print(output);
  salt_test();

}

void salt_test() {
  if (output > 0 && output <= 300) // High salt, hence, high conductivity.
  {

    leds[0] = CRGB::Red;
    FastLED.show();

    lcd.setCursor(0, 1);
    lcd.print("Not pure water");
  }
  else if (output > 300 && output < 600) // Low salt, hence, low conductivity.
  {
    leds[0] = CRGB::Green;
    FastLED.show();
    lcd.setCursor(0, 1);
    lcd.print("Pure water    ");

  }
  else
  {
    leds[0] = CRGB::Blue; // Sensor value does not fall within given range, so sensor is not in contact with water.
    FastLED.show();

    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Water Game:");
    lcd.setCursor(0, 1);
    lcd.print("Dip sensor    ");
  }
}