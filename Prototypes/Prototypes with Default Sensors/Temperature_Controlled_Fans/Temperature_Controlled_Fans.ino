/* Prototype Name : Temperature controlled Fans
   Input Sensor   : DHT22 Sensor
   Output Device  : Relay, LCD
   Libraries Used : DHT22, Adafruit Sensor, LiquidCrystalPCF8574
   FastLED to turn off from use
*/

#include <DHT.h>
#include <LiquidCrystal_PCF8574.h>
LiquidCrystal_PCF8574 lcd(0x27); //change the adress to 0x3F if necessary

#include<FastLED.h>
#define DATA_PIN 9 // Digital pin number for LED (Multicolor)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];
#define DHTPIN 7  // what pin we're connected to
#define RELAYPIN 4
#define DHTTYPE DHT22

DHT dht(DHTPIN, DHTTYPE);

void setup() {
  Serial.begin(9600);
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
  pinMode(RELAYPIN, OUTPUT);
  Serial.println("With Breadboard");
  dht.begin();
  lcd.begin(16, 2);
  lcd.setBacklight(255);
  lcd.home();
  lcd.clear();
  lcd.print("Auto fans");
  delay(5000);
}

void loop() {
  // Wait a few seconds between measurements.
  delay(2000);

  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();

  Serial.print("Humidity: ");
  Serial.print(h);
  Serial.println(" %\t");
  Serial.println("Temperature: ");
  Serial.println(t);
  Serial.print(" *C ");

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Temp(C):");
  lcd.print(t);
  if (t >= 24)
  {

    lcd.setCursor(0, 1);
    lcd.print("Its cold");
    digitalWrite(RELAYPIN, HIGH);
  }
  else
  {

    lcd.setCursor(0, 1);
    lcd.print("Its not warm");
    digitalWrite(RELAYPIN, LOW);
  }
}

