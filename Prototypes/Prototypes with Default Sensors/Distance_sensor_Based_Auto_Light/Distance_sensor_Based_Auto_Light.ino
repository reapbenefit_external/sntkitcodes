/* Prototype Name : Distance sensor  based Auto Light
   Input Sensor   : Distance Sensor
   Output Device  : Relay
   Libraries Used : LiquidCrystalPCF584 ,FastLED to turn off
*/

#include <LiquidCrystal_PCF8574.h>
LiquidCrystal_PCF8574 lcd(0x27); //Change accordingly
#include <FastLED.h>
#define DATA_PIN 9// Digital pin number for LED (NeoPixel)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];

#define trigPin 5 //Digital Pin 5
#define echoPin 6 //Digital Pin 6
int relay_pin = 4; //Digital Pin 4
long duration, cm, inches;

void setup() {
  Serial.begin (9600);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(relay_pin, OUTPUT);
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
  lcd.begin(16, 2);
  lcd.setBacklight(255);
  lcd.home(); lcd.clear();
  lcd.print("Auto Lights");
  delay(1000);
  lcd.clear();
}

void loop() {
  digitalWrite(trigPin, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
 
  // Read the signal from the sensor: a HIGH pulse whose
  // duration is the time (in microseconds) from the sending
  // of the ping to the reception of its echo off of an object.
  pinMode(echoPin, INPUT);
  duration = pulseIn(echoPin, HIGH);
 
  // Convert the time into a distance
  cm = (duration/2) / 29.1;     // Divide by 29.1 or multiply by 0.0343
  //inches = (duration/2) / 74;   // Divide by 74 or multiply by 0.0135
  
  //Serial.print(inches);
  //Serial.print("in, ");
  Serial.print(cm);
  Serial.print("cm");
  Serial.println();
  
  delay(250);

  if (cm > 1 && cm <= 22) {
    lcd.print("Move hand away");
    digitalWrite(relay_pin, LOW);
    delay(1000);
    lcd.clear();
  }
  else if(cm == 0){
    lcd.print("Move hand close");
    digitalWrite(relay_pin, HIGH);
    delay(1000);
    lcd.clear();
  }
  else {
    lcd.print("Move hand close");
    digitalWrite(relay_pin, HIGH);
    delay(1000);
    lcd.clear();
  }

}
