/* Prototype Name : Water Meter
   Input Sensor   : Water Flow Sensor
   Output Device  : LCD, Multicolor LED, Buzzer
   Libraries Used : LiquidCrystalPCF584, FastLED
*/


#include <Wire.h>
#include <LiquidCrystal_PCF8574.h>
LiquidCrystal_PCF8574 lcd(0x27); // change to 0X3F if necessary
#include <FastLED.h> //
#define DATA_PIN 9// Digital pin number for LED (NeoPixel)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];
#define buzzer 8
byte sensorInterrupt = 0;  // 0 = interrupt for digital pin 2
byte sensorPin       = 2;  //Digital Pin 2

// The hall-effect flow sensor outputs approximately 4.5 pulses per second per
// litre/minute of flow.
float calibrationFactor = 4.5;

volatile byte pulseCount;

float flowRate;
unsigned int flowMilliLitres;
unsigned long totalMilliLitres;

unsigned long oldTime;

void setup()
{
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();

  pinMode(buzzer, OUTPUT);

  lcd.begin(16, 2);
  // Initialize a serial connection for reporting values to the host
  Serial.begin(115200);
  pinMode(sensorPin, INPUT);
  digitalWrite(sensorPin, HIGH);

  pulseCount        = 0;
  flowRate          = 0.0;
  flowMilliLitres   = 0;
  totalMilliLitres  = 0;
  oldTime           = 0;

  attachInterrupt(sensorInterrupt, pulseCounter, FALLING);

  lcd.setBacklight(255);
  lcd.home(); lcd.clear();
  lcd.print("Hi:Water Meter");
  lcd.setCursor(0, 1);
  lcd.print("Conserve Water");
  delay(6000);

}

/**
   Main program loop
*/
void loop()
{

  if ((millis() - oldTime) > 1000)   // Only process counters once per second
  {
    leds[0] = CRGB::Black;
    FastLED.show();
    digitalWrite(buzzer, LOW);
    detachInterrupt(sensorInterrupt);
    flowRate = ((1000.0 / (millis() - oldTime)) * pulseCount) / calibrationFactor;
    oldTime = millis();
    flowMilliLitres = (flowRate / 60) * 1000;

    // Add the millilitres passed in this second to the cumulative total
    totalMilliLitres += flowMilliLitres;

    unsigned int frac;

    // Print the flow rate for this second in litres / minute
    Serial.print("Flow rate: ");
    Serial.print(int(flowRate));  // Print the integer part of the variable
    Serial.print(".");             // Print the decimal point
    // Determine the fractional part. The 10 multiplier gives us 1 decimal place.
    frac = (flowRate - int(flowRate)) * 10;
    Serial.print(frac, DEC) ;      // Print the fractional part of the variable
    Serial.print("L/min");
    // Print the number of litres flowed in this second
    Serial.print("  Current Liquid Flowing: ");             // Output separator


    // Print the cumulative total of litres flowed since starting
    Serial.print("  Output Liquid Quantity: ");             // Output separator
    // Reset the pulse counter so we can start incrementing again
    pulseCount = 0;

    // Enable the interrupt again now that we've finished sending output
    attachInterrupt(sensorInterrupt, pulseCounter, FALLING);
    // LCD , LED and buzzer
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Curr.Use(mL):");
    lcd.print(flowMilliLitres);
    lcd.setCursor(0, 1);
    lcd.print("Total(mL):");
    lcd.print(totalMilliLitres);

    if (flowMilliLitres > 2 && flowMilliLitres <= 49) {
      leds[0] = CRGB::Green;
      FastLED.show();
      delay(1000);
    }
    else if (flowMilliLitres >= 50) {
      digitalWrite(buzzer, HIGH);
//      delay(2000);
      leds[0] = CRGB::Red;
      FastLED.show();
      delay(1000);
    }
  }
}
/*
  Insterrupt Service Routine
*/
void pulseCounter()
{
  // Increment the pulse counter
  pulseCount++;
}
