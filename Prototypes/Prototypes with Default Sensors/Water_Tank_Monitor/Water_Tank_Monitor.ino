/* Prototype Name : Water Tank Monitor 
   Input Sensor   : Distance Sensor
   Output Device  : Buzzer, LCD
   Libraries Used : LiquidCrystalPCF584
   FastLED to turn off from use
*/


#include <LiquidCrystal_PCF8574.h>
LiquidCrystal_PCF8574 lcd(0x3F);
#include <FastLED.h>
#define DATA_PIN 9// Digital pin number for LED (NeoPixel)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];
#define TRIGGER 5
#define ECHO 6
#define buzzer 8
float duration, distance;

float l = 67;//length of tank in cm Sump - 205; mid - 67; top - 134;
float b = 66;//breadth of tank in cm Sump - 75; mid - 66; top - 95;
float maxlits = 400;//max capacity (l) of the tank to subtract from calculated value height sump - 173; mid - 93; top - 35;
float calclits;//calclulated litres not in tank
float litresleft;//whats in the tank litres
float value;

void setup() {
  Serial.begin (9600);
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
  pinMode(TRIGGER, OUTPUT);

  pinMode(ECHO, INPUT);

  pinMode(buzzer, OUTPUT);
  lcd.begin(16, 2);
  lcd.setBacklight(255);
  lcd.home(); lcd.clear();
  lcd.print("Water Tank alarm");
  delay(1000);
  lcd.clear();
}

void loop() {

  get_distance();
}


void get_distance() {

  digitalWrite(TRIGGER, LOW);
  delayMicroseconds(2);

  digitalWrite(TRIGGER, HIGH);
  delayMicroseconds(10);

  digitalWrite(TRIGGER, LOW);
  duration = pulseIn(ECHO, HIGH);
  distance = (duration / 2) / 29.1;

  //  Serial.print(distance);
  //  Serial.println("Centimeter:");

  value = distance;

  Serial.print(value);
  Serial.println("Centimeter:");
  delay(1000);

  calclits = ((value) * l * b * 0.001);
  Serial.println("Calculated Litres: ");
  Serial.println(distance);
  litresleft = maxlits - calclits;
  Serial.println("litresleft:");
  lcd.setCursor(0, 0);
  lcd.print("LtsLeft in Tank:");
  lcd.setCursor(0, 1);
  lcd.print(litresleft);
  beep();
}

void beep() {
  if (litresleft < 104.00) {
    tone(buzzer, 1000, 500);
    delay(10000);
  }

}




