/* Prototype Name : Vermicompost Monitor
   Input Sensor   : DHT22 Sensor
   Output Device  : Relay and Fan
   Libraries Used : FastLED
   LiquidCrystalPCF8574 to turn off from use
*/
#include <LiquidCrystal_PCF8574.h>
LiquidCrystal_PCF8574 lcd(0x3F); //Change accordingly
#include <FastLED.h>
#define DATA_PIN 9// Digital pin number for LED (NeoPixel)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];

#include "DHT.h"
#define DHTPIN 2     // what digital pin we're connected to
int relay = 4;
#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321
DHT dht(DHTPIN, DHTTYPE);


void setup() {
  Serial.begin(9600);
  Serial.println("DHTxx test!");
  pinMode(relay, OUTPUT); // declaring relay as the output
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
  lcd.begin(16, 2);
  lcd.setBacklight(255);
  lcd.home(); lcd.clear();
  lcd.print("Vermicompost");
  lcd.setCursor(0,1);
  lcd.print("Indicator");
  delay(1000);
  lcd.clear();
  dht.begin();
}

void loop() {
  // Wait a few seconds between measurements.
  delay(2000);
  float h = dht.readHumidity(); // finding only humidity measurements
//Based on humidity levels the fan will turn on to help in composting
  if (h < 60) {
    digitalWrite(relay, LOW); 
  }
  else {
    digitalWrite(relay, HIGH);
  }
}
