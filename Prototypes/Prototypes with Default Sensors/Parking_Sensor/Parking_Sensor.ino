/* Prototype Name : Parking Sensor
   Input Sensor   : Distance Sensor
   Output Device  : LCD, Multicolor LED
   Libraries Used : LiquidCrystalPCF584 ,FastLED
*/
#include <LiquidCrystal_PCF8574.h>
LiquidCrystal_PCF8574 lcd(0x3F); //Change accordingly
#include <FastLED.h>
#define DATA_PIN 9// Digital pin number for LED (NeoPixel)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];

#define trigpin 5
#define echopin 6

long duration, distance;

void setup() {
  Serial.begin(9600);
  pinMode(trigpin, OUTPUT);
  pinMode(echopin, INPUT);
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
  lcd.begin(16, 2);
  lcd.setBacklight(255);
  lcd.home(); lcd.clear();
  lcd.print("Parking Sensor");
  delay(1000);
  lcd.clear();
}
void reverse()
{
  if (distance < 10)
  {

    leds[0] = CRGB::Red;
    FastLED.show();
  }
  if (distance >= 100 || distance <= 10)
  {
    leds[0] = CRGB::Blue;
    FastLED.show();
  }
  else if (distance > 150)
  {
    leds[0] = CRGB::Green;
    FastLED.show();

  }

  delay(500);
}
void loop() {
  digitalWrite(trigpin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigpin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigpin, LOW);
  duration = pulseIn(echopin, HIGH);
  distance = duration / 58.2;
  Serial.print(distance);
  Serial.println("cm");
  reverse();

}


