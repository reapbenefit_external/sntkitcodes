/* Prototype Name : Heart Beat Monitor with KY039 sensor
   Input Sensor   : Heart Beat Sensor
   Output Device  : LCD
   Libraries Used : LiquidCrystalPCF584, 
   FastLED to turn off from Use
*/

#include <Wire.h>
#include <LiquidCrystal_PCF8574.h>

LiquidCrystal_PCF8574 lcd(0x3F); // change the address based on the LCD
#include<FastLED.h>
#define DATA_PIN 9 // Digital pin number for LED (Multicolor)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];

double alpha = 0.75;
int period = 20;
double refresh = 0.0;

void setup()
{
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
  pinMode(A2, INPUT);
  lcd.begin(16, 2);
  lcd.setBacklight(255);
  
}

void loop()
{
  static double oldValue = 0;
  static double oldrefresh = 0;

  int beat = analogRead(A2);

  double value = alpha * oldValue + (0 - alpha) * beat;
  refresh = value - oldValue;

  lcd.setCursor(0, 0);
  lcd.print(" Heart Monitor ");
  lcd.setCursor(0, 1);
  lcd.print("          ");
  lcd.setCursor(0, 1);
  lcd.print(beat / 10);
  oldValue = value;
  oldrefresh = refresh;
  delay(period * 10);
}
