/* Prototype Name : Intelligent Trash Can
   Input Sensor   : Moisture sensor, Servo Motor
   Output Device  : Multicolor LED
   Libraries Used : FastLED
   LiquidCrystalPCF8574 to turn off from use
*/

#include <LiquidCrystal_PCF8574.h>
LiquidCrystal_PCF8574 lcd(0x3F);  // set the LCD address accordingly
#include <FastLED.h>
#define DATA_PIN 9// Digital pin number for LED (NeoPixel)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];

int sensor_pin = A0; // select the input pin for the potentiometer

#include <Servo.h>
Servo servoDry; // Define our Servo
Servo servoWet; // Define our Servo


void setup() {
  // declare the ledPin as an OUTPUT:
  Serial.begin(9600);
  servoDry.attach(5); // servo on digital pin 5
  servoWet.attach(6); // servo on digital pin 6
  lcd.begin(16, 2); // initialize the lcd
  lcd.print("Intelligent Trash Can");
  delay(1000);
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();

}
void loop() {
  int value = analogRead(sensor_pin);
  Serial.println(value);


  if (value >= 1000)
  {
    servoDry.write(0);
    servoWet.write(0);
    delay(1000);          // Wait 1 second
  }
  else if (value < 1000 && value > 700)
  {
    leds[0] = CRGB::Red;
    FastLED.show();
    servoDry.write(270);
    delay(5000);          // Wait 1 second

  }
  else if (value <= 700) {
    leds[0] = CRGB::Green;
    FastLED.show();
    servoWet.write(270);
    delay(10000);
  }
}


