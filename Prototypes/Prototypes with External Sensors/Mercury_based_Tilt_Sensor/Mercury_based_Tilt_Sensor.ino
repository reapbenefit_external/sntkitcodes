/* Prototype Name : Mercury based Tilt Sensor
   Input Sensor   : Tilt Sensor
   Output Device  : On Board LED
   Libraries Used : LiquidCrystalPCF584,
   FastLED to turn off from Use
*/
#include <Wire.h>
#include <LiquidCrystal_PCF8574.h>

LiquidCrystal_PCF8574 lcd(0x27); // change the address based on the LCD
#include<FastLED.h>
#define DATA_PIN 9 // Digital pin number for LED (Multicolor)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];
int led_pin = 5;
int mercury_pin = 6;

void setup() {
  pinMode(led_pin, OUTPUT);
  pinMode(mercury_pin, INPUT);
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
  lcd.begin(16, 2);
  lcd.setBacklight(255);
  lcd.setCursor(0, 0);
  lcd.print("Tilt Sensor");

}

void loop() {
  if (digitalRead(6) == 1)
  {
    digitalWrite(5, HIGH);
    delay(300);
    else {
      digitalWrite(5, LOW);

      delay(300);
    }
  }
}
}
