/* Prototype Name : GPS Based Dust bin
   Input Sensor   : Distance sensor
   Output Device  : SIM800C Module with SIM card
   Libraries Used : Adafruit FONA Library
   LiquidCrystalPCF584 , FastLED to turn off from use
*/
 
#include <Adafruit_FONA.h> // https://github.com/adafruit/Adafruit_FONA
#include <SoftwareSerial.h>
#define FONA_RX 2 // flow sensor Data pin
#define FONA_TX 3 // DHT sensor Data pin
SoftwareSerial fona (FONA_TX, FONA_RX);
char replybuffer[255];

int timetaken, dist;

#define trigPin 5 // Arduino pin tied to trigger pin on the ultrasonic sensor.
#define echoPin 6 // Arduino pin tied to echo pin on the ultrasonic sensor.

void setup() {
  Serial.begin(9600);
  fona.begin(9600);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);

  digitalWrite(trigPin, HIGH);
  delayMicroseconds(1000);
  digitalWrite(trigPin, LOW);
  timetaken = pulseIn(echoPin, HIGH);

  
  // fona.print(BinID);
  dist = (timetaken / 2) * 0.034049 ;
  
}
void loop() {
 if (dist <= 10) {
    fona.print("AT+CMGF=1\r");
    fona.print("AT+CMGS=\"+91xxxxxxxx\"\r"); // Enter your mobile number. Use3G SIM in the SIM module
    fona.print("#its near full!!");
    fona.print("\r");
    Serial.print("text");
    delay(1000);
    fona.println((char)26);
    fona.println();
    Serial.println("mssg sent");
    delay(10000);
  }
  else if (dist >= 200 || dist < 10) {
    fona.print("# Half full");
    fona.print("\r");
    delay(1000);
    fona.println((char)26);
    fona.println();
    Serial.print( dist);
    delay(1000);
  }
  else {
    fona.print("#Empty");
    fona.print("\r");
    delay(1000);
    fona.println((char)26);
    fona.println();
    Serial.print( dist);
    delay(1000);
  }

}


