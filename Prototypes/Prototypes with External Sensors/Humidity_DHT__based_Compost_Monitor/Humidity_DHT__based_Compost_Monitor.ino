/* Prototype Name : Humidity based Compost Monitor
   Input Sensor   : 2 DHT22
   Output Device  : Buzzer, LCD, Multicolor LED
   Libraries Used : LiquidCrystalPCF584,FastLED
*/


#include <DHT.h>
#include<Adafruit_Sensor.h>
#include<FastLED.h>
#include <LiquidCrystal_PCF8574.h>
LiquidCrystal_PCF8574 lcd(0x3F);
#define DATA_PIN 9 // Digital pin number for LED (Multicolor)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];
#define buzzer 8
#define DHT1PIN 2     // Inside the bin
#define DHT2PIN 7 // Outside the bin

// Uncomment whatever type you're using!
#define DHT1TYPE DHT22  // DHT 11 
#define DHT2TYPE DHT22   // DHT 22  (AM2302)
//#define DHTTYPE DHT21   // DHT 21 (AM2301)

// Connect pin 1 (on the left) of the sensor to +5V
// Connect pin 2 of the sensor to whatever your DHTPIN is
// Connect pin 3 (on the right) of the sensor to GROUND
int tempin, tempout, humin, humout;

DHT dht1(DHT1PIN, DHT1TYPE); // Inside the bin
DHT dht2(DHT2PIN, DHT2TYPE); //Outside the bin

void setup() {
  Serial.begin(9600);
  Serial.print("DHT test");
  dht1.begin();
  dht2.begin();
  pinMode(buzzer, OUTPUT);
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
  delay(1000);
  lcd.begin(16, 2);
  lcd.setBacklight(255);
  lcd.home(); lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Compost Monitor:");
  delay(4000);

}

void loop() {

  delay(4000);
  compost_test();
}
void dht_test() {
  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  humin = dht1.readHumidity();
  tempin = dht1.readTemperature();
  humout = dht2.readHumidity();
  tempout = dht2.readTemperature();

  // check if returns are valid, if they are NaN (not a number) then something went wrong!
  if (isnan(tempin) || isnan(humin)) {
    Serial.println("Failed to read from DHT #1");
  } else {
    Serial.print("Humidity 1: ");
    Serial.print(humin);
    Serial.print(" %\t");
    Serial.print("Temperature 1: ");
    Serial.print(tempin);
    Serial.println(" *C");
  }
  if (isnan(tempout) || isnan(humout)) {
    Serial.println("Failed to read from DHT #2");
  } else {
    Serial.print("Humidity 2: ");
    Serial.print(humout);
    Serial.print(" %\t");
    Serial.print("Temperature 2: ");
    Serial.print(tempout);
    Serial.println(" *C");
  }
  Serial.println();
}
void compost_test() {
  dht_test();
  if ( humout < humin) {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("BinHum(%):");
    lcd.print(humin);
    lcd.setCursor(0, 1);
    lcd.print("Hum(*%):");
    lcd.print(humout);
    lcd.setCursor(10, 1);
    lcd.print(" CIP");
    leds[0] = CRGB::Red;
    FastLED.show();
    delay(2000); // Red light glows for 1 second

  }
  else if (humout >= humin) {
    lcd.clear();
    lcd.setCursor(0, 0);
    //Serial.print(temp_0);
    lcd.print("BinHum(%):");
    lcd.print(humin);
    lcd.setCursor(0, 1);
    lcd.print("Hum(%):");
    lcd.print(humout);
    lcd.setCursor(10, 1);
    lcd.print(" Done");
    leds[0] = CRGB::Green;
    FastLED.show();
    delay(2000); // Red light glows for 1 second
    tone(buzzer, 1000, 500) ;//beeps at frequency of 1000Hz
    delay(1000);
  }
}
