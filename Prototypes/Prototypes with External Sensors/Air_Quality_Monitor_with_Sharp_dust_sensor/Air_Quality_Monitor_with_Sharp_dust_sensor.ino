/* Prototype Name : Air Quality station
   Input Sensor   : DHT 22, Sharp Dust Sensor
   Output Device  : LCD, MulticolorLED
   Libraries Used : LiquidCrystalPCF584, FastLED
*/
#include <DHT.h>

#include <LiquidCrystal_PCF8574.h>
LiquidCrystal_PCF8574 lcd(0x3F); //change the address if necessary
#include<FastLED.h>
#define DATA_PIN 9 // Digital pin number for LED (Multicolor)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];

int measurePin = 0; //Analog pin 0
int ledPower = 3; //Digital pin 3
int dhtpin = 7; //Digital pin 7
#define DHTTYPE DHT22
DHT dht (dhtpin, DHTTYPE);

unsigned int samplingTime = 280;
unsigned int deltaTime = 40;
unsigned int sleepTime = 9680;

float voMeasured = 0;
float calcVoltage = 0;
float dustDensity = 0;

int humidity, temperature;

void setup() {
  Serial.begin(115200);
  Wire.begin();
  pinMode(ledPower, OUTPUT);
  pinMode(dhtpin, OUTPUT);
  delay(1000);
  dht.begin();
  lcd.home();   // initializing the LCD
  lcd.begin(16, 2); // initialize the lcd
  lcd.setBacklight(200);
  lcd.home(); lcd.clear();
  lcd.print("Know Air Quality");
  delay(1000);
  lcd.clear();
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
}

void loop() {

  dust_sensor();
  temp_sensor();
  showlcd();
}
void dust_sensor() {
  delay(15000);
  digitalWrite(dhtpin, HIGH);
  digitalWrite(ledPower, LOW); // power on the LED
  delayMicroseconds(samplingTime);
  voMeasured = analogRead(measurePin); // read the dust value
  delayMicroseconds(deltaTime);
  digitalWrite(ledPower, HIGH); // turn the LED off
  delayMicroseconds(sleepTime);
  calcVoltage = voMeasured * (5.0 / 1024);

  // linear eqaution taken from http://www.howmuchsnow.com/arduino/airquality/
  // Chris Nafis (c) 2012
  //  dustDensity = (0.17 * calcVoltage - 0.1) * 1000;
  dustDensity = (0.17 * calcVoltage) * 1000;

  Serial.print("Raw: ");
  Serial.print(voMeasured);

  Serial.print("; - Voltage: ");
  Serial.print(calcVoltage);

  Serial.print("; - Dust Density [ug/m3]: ");
  Serial.print(dustDensity);
  Serial.println(";");
  delay(1000);
}
void temp_sensor() {
  delay(2000);
  humidity = dht.readHumidity();
  temperature = dht.readTemperature();
}
void showlcd() {
  lcd.setCursor(0, 0);

  lcd.print("T(C): ");
  lcd.print(temperature);
  lcd.print(" H(%): ");
  lcd.print(humidity);

  lcd.setCursor(0, 1);
  lcd.print("PM2.5: ");
  lcd.print(dustDensity);
  lcd.print("ug/m3");

}

