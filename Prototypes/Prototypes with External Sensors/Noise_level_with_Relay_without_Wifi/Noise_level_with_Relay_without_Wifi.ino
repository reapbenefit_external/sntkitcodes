/* Prototype Name : Noise level with relay 
   Input Sensor   : Analog Sound Level sensor, Relay
   Output Device  : Relay, LiquidCrystalPCF584
   Libraries Used : LiquidCrystalPCF584
   FastLED to turn off from use
*/

#include <LiquidCrystal_PCF8574.h>
LiquidCrystal_PCF8574 lcd(0x27);

#include <FastLED.h>
int relay_pin = 4 ; // Relay digital pin D4

#define DATA_PIN 9// Digital pin number for LED (NeoPixel)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];


//dB values
const float dBAnalogQuiet = 10; // calibrated value from analog input (48 dB equivalent)
const float dBAnalogMedium = 12;
const float dBAnalogLoud = 15;

#define PIN_ANALOG_IN A2

void setup()
{
  pinMode(relay_pin, OUTPUT); //pin connected to the relay
  Serial.begin(9600);

  pinMode(A2, INPUT);
  lcd.begin(16, 2);
  lcd.setBacklight(255);
  lcd.print("Nosie Level Test");
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
}

void loop()
{
  int value;
  //Decibels values
  float decibelsValueQuiet = 49;
  float decibelsValueMedium = 65;
  float decibelsValueLoud = 70;

  value = analogRead(PIN_ANALOG_IN);
  Serial.print(value);

  if ((value > 0) && (value <= 10))
  {
    decibelsValueQuiet += 20 * log10(value / dBAnalogQuiet);
    printDecibels("Quiet :)", decibelsValueQuiet);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Less Noise(dB):");
    lcd.setCursor(0, 1);
    lcd.print(decibelsValueQuiet);
    delay (1000);
  }
  else if ((value >= 11) && ( value <= 14) )
  {
    decibelsValueMedium += log10(value / dBAnalogMedium);
    printDecibels("Medium", decibelsValueMedium);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Medium Noise(dB):");
    lcd.setCursor(0, 1);
    lcd.print(decibelsValueMedium);
    delay (100);
  }
  else if (value >= 15)
  {
    decibelsValueLoud += log10(value / dBAnalogLoud);
    printDecibels("Loud :(", decibelsValueLoud);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Be Quiet(dB):");
    lcd.setCursor(0, 1);
    lcd.print(decibelsValueLoud);
    delay (1000);
    digitalWrite (relay_pin, HIGH);
    delay(1000);
  }
  delay(1000);
}

void printDecibels (const char* volume, float dBs)
{

  Serial.print(volume);

  Serial.println(dBs);
}
