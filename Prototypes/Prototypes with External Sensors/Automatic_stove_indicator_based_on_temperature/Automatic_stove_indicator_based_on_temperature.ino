/* Prototype Name : Automatic Stove
   Input Sensor   : DS18b20 Temperature Sensor
   Output Device  : Buzzer
   Libraries Used : LiquidCrystalPCF584 and FastLED to turn off from use. 
*/

//Include libraries
#include <LiquidCrystal_PCF8574.h>
LiquidCrystal_PCF8574 lcd(0x27); //Change to 0x3F if necessary
#include <FastLED.h>
#define DATA_PIN 7// Digital pin number for LED (NeoPixel)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];
#include <OneWire.h>
#include <DallasTemperature.h>

// Data wire is plugge`d into pin 2 on the Arduino
#define ONE_WIRE_BUS 3 //Digital Pin 3
// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);
// Pass our oneWire reference to Dallas Temperature.
DallasTemperature sensors(&oneWire);

int sensor_pin = A0; // analog pin A0
int Buzzer = 8;

void setup(void)
{
  Serial.begin(9600); //Begin serial communication
  lcd.begin(16, 2);
  lcd.setBacklight(255);
  lcd.setCursor(0,0);
  lcd.print("This is the Smart Stove Solution"); //Print a message
  Serial.println("Reading From the TILT Sensor ...");
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
  delay(1000);
  pinMode(Buzzer, OUTPUT);
  sensors.begin();
}

void loop(void)
{
  int value = analogRead(sensor_pin);
  //value = value / 10;
  if (value > 300)
  {
    lcd.print("Stove is On");
    Serial.println ("Detecting the temperature near the burner");
    Serial.println ("Wait for 15 Seconds");

    delay(15000);
    // Send the command to get temperatures
    sensors.requestTemperatures();
    lcd.clear();
    lcd.setCursor(0,0);
    Serial.print("Temperature is: ");
    lcd.print(sensors.getTempCByIndex(0)); // Why "byIndex"? You can have more than one IC on the same bus. 0 refers to the first IC on the wire
    int Temp = sensors.getTempCByIndex(0);
    if (Temp > 35)
    {
      Serial.println ("Temperature Detected");
      digitalWrite(Buzzer, LOW);

    }
    else
    {
      Serial.println ("HIGH Temperature not detected");
      Serial.println ("Stove is on but It is not lit(Indicating Gas leakage)");
      digitalWrite(Buzzer, HIGH);
    }
  }
  else
  {
    Serial.println("Stove is Off");
    digitalWrite(Buzzer, LOW);
    delay(1000);
  }
}
