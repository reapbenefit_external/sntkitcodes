/* Prototype Name : Know the Noise Level
   Input Sensor   : Analog Sound Level sensor
   Output Device  : Buzzer, LiquidCrystalPCF584
   Libraries Used : LiquidCrystalPCF584
   FastLED to turn off from use
*/

#include <LiquidCrystal_PCF8574.h>
LiquidCrystal_PCF8574 lcd(0x3F);
#include <FastLED.h>

#define DATA_PIN 9// Digital pin number for LED (NeoPixel)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];


//dB values
const float dBAnalogQuiet = 10; // calibrated value from analog input (48 dB equivalent)
const float dBAnalogMedium = 12;
const float dBAnalogLoud = 17;

#define PIN_ANALOG_IN A2

void setup()
{

  Serial.begin(9600);

  pinMode(A2, INPUT);
  lcd.begin(16, 2);
  lcd.setBacklight(255);
  lcd.print("Nosie Level indicator");
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
}

void loop()
{
  int value;
  //Decibels values
  float decibelsValueQuiet = 49;
  float decibelsValueMedium = 65;
  float decibelsValueLoud = 70;

  value = analogRead(PIN_ANALOG_IN);
  Serial.print(value);

  if (value < 10)
  {
    decibelsValueQuiet += 20 * log10(value / dBAnalogQuiet);
    printDecibels("Quiet :)", decibelsValueQuiet);
  }
  else if ((value >=10) && ( value <= 17) )
  {
    decibelsValueMedium += log10(value / dBAnalogMedium);
    printDecibels("Medium", decibelsValueMedium);
  }
  else if (value > 17)
  {
    decibelsValueLoud += log10(value / dBAnalogLoud);
    printDecibels("Loud :(", decibelsValueLoud);
  }
  delay(1000);
}

void printDecibels (const char* volume, float dBs)
{

  Serial.print(volume);

  Serial.println(dBs);
}
