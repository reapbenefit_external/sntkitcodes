/* Prototype Name : Temperature Monitor with LM25 Temperature Sensor
   Input Sensor   : LM35 Temperature Sensor
   Output Device  : LCD
   Libraries Used : LiquidCrystalPCF584,
   FastLED to turn off from Use
*/
#include <Wire.h>
#include <LiquidCrystal_PCF8574.h>

LiquidCrystal_PCF8574 lcd(0x27); // change the address based on the LCD
#include<FastLED.h>
#define DATA_PIN 9 // Digital pin number for LED (Multicolor)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];

const int sensor = A2; // Assigning analog pin A1 to variable 'sensor'
float tempc;  //variable to store temperature in degree Celsius
float tempf;  //variable to store temperature in Fahreinheit
float vout;  //temporary variable to hold sensor reading
void setup()
{
  pinMode(sensor, INPUT); // Configuring pin A1 as input
  Serial.begin(9600);
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
  lcd.begin(16, 2);
  lcd.setBacklight(255);
}
void loop()
{
  vout = analogRead(sensor);
  vout = (vout * 500) / 1023;
  tempc = vout; // Storing value in Degree Celsius
  tempf = (vout * 1.8) + 32; // Converting to Fahrenheit
  Serial.print("in DegreeC=");
  Serial.print("\t");
  Serial.print(tempc);
  Serial.println();
  Serial.print("in Fahrenheit=");
  Serial.print("\t");
  Serial.print(tempf);
  Serial.println();
  delay(1000); //Delay of 1 second for ease of viewing
  lcd.setCursor(0, 0);
  lcd.print("Temp(C):");
  lcd.print(tempc);
  delay(1000);
}
