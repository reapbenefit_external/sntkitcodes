/* Prototype Name : Vehicle Emission Monitor
   Input Sensor   : MQ7 Sensor (External Sensor not inculded in Kit)
   Output Device  : LCD, Multicolor LED
   Libraries Used : LiquidCrystalPCF584, FastLED
*/
#include <LiquidCrystal_PCF8574.h>
LiquidCrystal_PCF8574 lcd(0x27); // address of LCD
#include<FastLED.h>

int mq7_sensor = A0; //Carbon Monoxide sensor connected to Analog A0 pin
#define DATA_PIN 9 // Digital pin number for LED (Multicolor)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
  lcd.begin(16, 2);
  lcd.setBacklight(255);
  lcd.home();
  lcd.setCursor(0, 0);
  lcd.print("Vehicle");
  lcd.setCursor(0, 1);
  lcd.print("Emission Test");
  delay(2000);
  lcd.clear();
}

void loop() {
  // put your main code here, to run repeatedly:
  int val;
  val = analogRead(sensor);
  Serial.println(val, DEC);
  delay(1000);
  lcd.setCursor(0, 0);
  lcd.print("CO Level(ppm):");
  lcd.setCursor(0, 1);
  lcd.print(val, DEC);
  if (val > 300) {
    lcd.setCursor(4, 2);
    lcd.print("Do emission Test");
    leds[0] = CRGB::Red;
    FastLED.show();
  }
  else {
    lcd. setCursor(4, 2);
    lcd.print("Safe"            );
    leds[0] = CRGB::Green;
    FastLED.show();
  }
}
