/* Prototype Name : Swimming Pool Monitor(Temperature inside Pool & outside pool)
   Input Sensor   : DHT22, DS18b20 Sensor
   Output Device  : LCD, Multicolor LED
   Libraries Used : LiquidCrystalPCF584, One wire, Dallas Temperature, FastLED
*/

#include <OneWire.h>
#include <DallasTemperature.h>
#include <LiquidCrystal_PCF8574.h>
LiquidCrystal_PCF8574 lcd(0x3F);
#include<DHT.h>
#include<FastLED.h>

DHT dht (7, DHT22); // 7 denotes Digital pin for DHT22 sensor, DHT22 denotes DHT type
#define DATA_PIN 9 // Digital pin number for LED (Multicolor)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];
#define ONE_WIRE_BUS 2

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);

int tempC = 0;
int humidity = 0;
int temperature = 0;
void setup() {
  sensors.begin();
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  lcd.begin(16, 2);
  lcd.setBacklight(255);
  lcd.home();
  lcd.clear();
  dht.begin();
  lcd.setCursor(0, 0);
  lcd.print("Pool Monitor:");
  lcd.setCursor(0, 1);
  lcd.print("With SNT kit");
  delay(4500);
  Serial.begin(9600);
}
void loop() {
  dsb();
  dht22();
  leds[0] = CRGB::Black;
  FastLED.show();
}
void dsb() {
  sensors.requestTemperatures();
  tempC = sensors.getTempCByIndex(0);
  delay(1000);
  Serial.println(tempC);

  lcd.setCursor(0, 0);
  lcd.print("PoolTemp(C):");
  lcd.print(tempC);
  lcd.print("*C");
}
void dht22() {
  delay(2000);
  humidity = dht.readHumidity();
  temperature = dht.readTemperature();

  lcd.setCursor(0, 1);
  lcd.print("T(C):");
  lcd.print(temperature);
  lcd.print("H(%)");
  lcd.print(humidity);
}

