/* Prototype Name : Cannal Communications(Irrigation system for Farmers)
   Input Sensor   : Soil Moisture sensor, Rain sensor
   Output Device  : SIM800C / SIM800L Module with SIM card
   Libraries Used : Adafruit FONA Library
   LiquidCrystalPCF584 , FastLED to turn off from use
*/
#include <LiquidCrystal_PCF8574.h>
LiquidCrystal_PCF8574 lcd(0x3F);
#include <Adafruit_FONA.h>
#include <SoftwareSerial.h>
#include<FastLED.h>
#define DATA_PIN 9 // Digital pin number for LED (Multicolor)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];

int sensorpin = A0; //  moisture sensor
int rainpin = A2; //rain sensor

#define FONA_RX 5
#define FONA_TX 6
#define FONA_RST 3
SoftwareSerial fona (FONA_TX, FONA_RX);

char replybuffer[255];

void setup() {
  Serial.begin(9600);
  fona.begin(9600);
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
  pinMode(sensorpin, INPUT);
  pinMode(rainpin, INPUT);
  lcd.begin(16, 2);
  lcd.setBacklight(255);
  lcd.setCursor(0, 0);
  lcd.print("Cannal Comm.");
  lcd.clear();

}
void loop() {
  rain();
  soil();
}
void rain() {
  int rainSenseReading = analogRead(rainpin);
  Serial.println(rainSenseReading); // serial monitoring message
  delay(250);// rain sensing value from 0 to 1023 : from heavy rain - no rain.

  if (rainSenseReading > 350 && rainSenseReading < 1000) {
    fona.print("AT+CMGF=1\r");
    fona.print("AT+CMGS=\"+91xxxxxxxx\"\r"); // Enter your mobile number. Use3G SIM in the SIM module
    fona.print("its Raining!");
    Serial.print("its Raining!");
    fona.print("\r");
    Serial.print("text");
    delay(1000);
    fona.println((char)26);
    fona.println();
    Serial.println("mssg sent");
    delay(10000);
  }
  else if (rainSenseReading < 350) {
    fona.print("AT+CMGF=1\r");
    fona.print("AT+CMGS=\"+91xxxxxxxx\"\r"); // Enter your mobile number. Use3G SIM in the SIM module
    fona.print("Half rain");
    Serial.print("Half rain");
    fona.print("\r");
    delay(1000);
    fona.println((char)26);
    fona.println();

    delay(1000);
  }
  else {
    fona.print("AT+CMGF=1\r");
    fona.print("AT+CMGS=\"+91xxxxxxxx\"\r"); // Enter your mobile number. Use3G SIM in the SIM module
    fona.print("No rain");
    Serial.print("No rain ");
    fona.print("\r");
    delay(1000);
    fona.println((char)26);
    fona.println();

    delay(1000);
  }
}
void soil() {
  int value = analogRead(sensorpin);
  Serial.println(value);
  delay(2000);

  if ( value < 400)
  {
    Serial.println("Wet soil");
    fona.print("Wet soil, no water required");
    fona.print("\r");
    delay(1000);
    fona.println((char)26);
    fona.println();

  }
  else
  {
    Serial.println("Dry soil"); // Greater than 400
    fona.print("Dry soil, water required");
    fona.print("\r");
    delay(1000);
    fona.println((char)26);
    fona.println();
    delay(2000);
  }
}

