/* Prototype Name : Air Quality station
   Input Sensor   : DHT 22, Honeywell HPM dust Sensor
   Output Device  : LCD, MulticolorLED
   Libraries Used : LiquidCrystalPCF584, FastLED ,HPMA115s0
*/
#include <hpma115S0.h>
#include <SoftwareSerial.h>
//Create an instance of software serial
SoftwareSerial hpmaSerial(5, 6); // Feather TX, Feather RX
//Create an instance of the hpma115S0 library
HPMA115S0 hpma115S0(hpmaSerial);

#include <LiquidCrystal_PCF8574.h>

#include <DHT.h>
LiquidCrystal_PCF8574 lcd(0x3F);
DHT dht(7, DHT22);
int humidity, temperature;

#include<FastLED.h>
#define DATA_PIN 9 // Digital pin number for LED (Multicolor)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];
unsigned int pm2_5, pm10;

void setup() {
  Serial.begin(9600);
  hpmaSerial.begin(9600);
  delay(5000);
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
  Serial.println("Starting...");
  hpma115S0.Init();
  hpma115S0.StartParticleMeasurement();
  dht.begin();
  lcd.begin(16, 2);
  lcd.setBacklight(255);
  lcd.print("Know Air Quality:");
  delay(2000);
  lcd.clear();
}

void loop() {
  hpm_serial();
  hpm_lcd();
  temp();
  rgb();
}
void temp() {
  delay(2000);
  humidity = dht.readHumidity();
  temperature = dht.readTemperature();
  Serial.print("\n Humidity =");
  Serial.print(humidity);
  Serial.print("% \n");
  Serial.print("Temperature =");
  Serial.print(temperature);
  Serial.print(" *C");
}
void hpm_serial() {
  if (hpma115S0.ReadParticleMeasurement(&pm2_5, &pm10)) {
    Serial.println("PM 2.5: " + String(pm2_5) + " ug/m3" );
    Serial.println("PM 10: " + String(pm10) + " ug/m3" );
  }
  delay(1000);
}
void hpm_lcd() {
  lcd.setBacklight(255);
  lcd.setCursor(0, 0);
  lcd.print("T(C):");
  lcd.print(temperature);
  //lcd.setCursor(8,0);
  lcd.print("  H(%):") ;
  lcd.print(humidity);
  lcd.setCursor(0, 1);
  lcd.print("PM 2.5:");
  lcd.print(String(pm2_5));
  lcd.print(" ug/m3");
  if (pm2_5 >= 0 && pm2_5 <= 75) {
    lcd.setCursor(10, 1);
    lcd.print("Good..");
    delay(1000);
  } else if (pm2_5 >= 76 && pm2_5 <= 1000) {
    lcd.setCursor(10, 1);
    lcd.print("Bad....");
    delay(1000);
  }
}
void rgb() {
  if (pm2_5 <= 75) {
    leds[0] = CRGB::Green;
    FastLED.show();
    delay(1000);
  }
  else if (pm2_5 >= 76 && pm2_5 <= 1000) {
    leds[0] = CRGB::Red;
    FastLED.show();
    delay(1000);
  }
}
