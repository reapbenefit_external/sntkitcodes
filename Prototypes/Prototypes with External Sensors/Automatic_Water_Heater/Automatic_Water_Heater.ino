/* Prototype Name : Automatic Water Heater
   Input Sensor   : DS18b20 Temperature Sensor(External sensor not included in kit)
   Output Device  : Relay
   Libraries Used : One wire, DallasTemperature for measuring Temperature values
   And LiquidCrystalPCF584, FastLED to turn off from use
*/

#include <OneWire.h>
#include <DallasTemperature.h>
#include <LiquidCrystal_PCF8574.h>
LiquidCrystal_PCF8574 lcd(0x3F); //Change address to 0x27 if needed
#include<FastLED.h>

int relay_pin = 4;
#define DATA_PIN 9 // Digital pin number for LED (Multicolor)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];
#define ONE_WIRE_BUS 2
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
int tempC = 0;

void setup() {
  sensors.begin();
  pinMode(relay_pin, OUTPUT);
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  lcd.begin(16, 2);
  lcd.setBacklight(0);
  lcd.home();
  lcd.clear();
  Serial.begin(9600);
}
void loop() {
  dsb();
  heater_test();
  leds[0] = CRGB::Black;
  FastLED.show();
}
void dsb() {
  sensors.requestTemperatures();
  tempC = sensors.getTempCByIndex(0);
  delay(1000);
  Serial.println(tempC);
}

void heater_test() {
  if (tempC > 40) {
    digitalWrite(relay, LOW); // Turns off the heater
  }
}
