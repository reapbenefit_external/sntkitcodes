/* Prototype Name : Breathanalyzer
   Input Sensor   : MQ3 Sensor
   Output Device  : LCD, Buzzer
   Libraries Used : LiquidCrystalPCF584
   FastLED to turn off from use
*/
#include <LiquidCrystal_PCF8574.h>
LiquidCrystal_PCF8574 lcd(0x3F); //change to 0x27 if necessary
#include <FastLED.h>

#define DATA_PIN 9// Digital pin number for LED (NeoPixel)
#define NUM_LEDS 3
CRGB leds[NUM_LEDS];

#define sensor A0
#define buzzer 8

void setup()
{
  Serial.begin(9600);
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  leds[0] = CRGB::Black;
  FastLED.show();
  lcd.begin(16, 2);
  lcd.print("Alcohol Detector");
  delay(2000);
  pinMode(sensor, INPUT);
  pinMode(buzzer, OUTPUT);
  lcd.clear();
}

void loop()
{
  float Value = 0;
  for (int i = 0; i < 10; i++)
  {
    Value += analogRead(sensor);
    delay(10);
  }
  float v = (Value / 10) * (5.0 / 1024.0);
  float mgL = 0.67 * v; // factor * voltage
  Serial.print("BAC:");
  Serial.print(mgL);
  Serial.print(" mg/L");
  lcd.setCursor(0, 0);
  lcd.print("BAC: "); // Blood alcohol concentration
  lcd.print(mgL);
  lcd.print(" mg/L        ");
  lcd.setCursor(0, 1);
  if (mgL > 0.8)
  {
    lcd.print("Drunk   ");
    Serial.println("Drunk");
    digitalWrite(buzzer, HIGH);
  }
  else
  {
    lcd.print("Normal  ");
    Serial.println("Normal");
    digitalWrite(buzzer, LOW);
  }

  delay(100);

}
